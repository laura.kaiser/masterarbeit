import os
import pandas
import csv
import bs4
import re

file_dir = os.path.abspath(os.path.dirname(__file__))

tei_in = 'manuskripte_tei.xml'

tei_in_file = open(os.sep.join([file_dir,'input',tei_in]),encoding='utf-8',mode='r')
tei_data = tei_in_file.read()
tei_in_file.close()

out_dir = os.sep.join([file_dir,'output'])
os.makedirs(out_dir,exist_ok=True)
data_out = open(os.sep.join([out_dir,'data.csv']),encoding='utf-8',mode='w',newline='')


data_tei = bs4.BeautifulSoup(tei_data,'xml')

data_csv = csv.DictWriter(data_out,fieldnames=['keywords','year','_medizin','_personen','_tiere','_objekte','_ethngrp','_messungen','_geo','_pflanzen','_gestminer','_zitate','_einheiten','_lawihw','_zeichnungen','id','_text'],delimiter=',')

data_csv.writeheader()

data = []

col_elements = {'_medizin':[['seg','type','medicine'],'Medizin'],'_personen':[['name','type','person'],'Personen'],'_tiere':[['name','type','animal'],'Tiere'],'_objekte':[['name','type','object'],'Objekte'],'_ethngrp':[['name','type','ethnicity_group'],'Ethnien/Gruppen'],'_messungen':[['seg','type','measurement-data'],'Messungen'],'_geo':[['name','type','geographic'],'Geographische Angaben'],'_pflanzen':[['name','type','plant'],'Pflanzen'],'_gestminer':[['name','type','rock_mineral'],'Gesteine/Minerale'],'_zitate':[['quote'],'Zitate'],'_einheiten':[['unit'],'Maßeinheiten'],'_lawihw':[['seg','type','agriculture_crafts'],'Landwirtschaft und Handwerk'],'_zeichnungen':[['figure'],'Zeichnung'],'_text':''}


year_mapping = {'01':'1822','02':'1825','03':'1825','04':'1826','05':'1826','06':'1826','07':'1826','08':'1827','09':'1828','10':'1828','11':'1828','12':'1828','13':'1828','14':'1828','15':'1828','16':'1828','17':'1828','18':'1828','19':'1830','20':'1830','21':'1830','22':'1830','23':'1831','24':'1826','25':'1823','26':'1828'}


def element_from_key(tag):
    if tag.name == col_elements[key][0][0] and tag[col_elements[key][0][1]] == col_elements[key][0][2]:
        return True
    else:
        return False


def get_val_string(values):
    if isinstance(values,set):
        val_string = re.sub('-(\s| )*?\n(\s| )*','',', '.join(values),re.S)
        return val_string.replace('\n',' ')
    else:
        return values.replace('\n',' ')


for ab in data_tei.find_all('ab'):
    # Dict für Tagsammlung pro Seite
    ab_data = {'keywords':set(),'year':set(),'_medizin':set(),'_personen':set(),'_tiere':set(),'_objekte':set(),'_ethngrp':set(),'_messungen':set(),'_geo':set(),'_pflanzen':set(),'_gestminer':set(),'_zitate':set(),'_einheiten':set(),'_lawihw':set(),'_zeichnungen':set(),'id':set()}
    # Jahr entsprechend Entstehung der Berichte zuordnen
    ab_data['year'] = year_mapping[ab.find_parent('text')['n']]
    # IDs für Faksimile-Anzeige der aufbereiteten Grafiken
    if re.search('(?<=http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/).+?(?=.tif)',ab.find_previous('pb')['facs']):
        ab_data['id'] = re.search('(?<=http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/).+?(?=.tif)',ab.find_previous('pb')['facs']).group(0)
    else:
        ab_data['id'] = ab.find_previous('pb')['xml:id'] + '_NA'
    # für jedes interessierende Element Textinhalt und Kategorienamen zu Stichwörtern und entsprechender Kategoriespalte hinzufügen:
    for key in col_elements:
        if key != '_text':
            # Abfrage für Elemente ohne Attribute
            if len(col_elements[key][0]) == 1:
                for element in ab.find_all(col_elements[key][0][0]):
                    ab_data[key].add(element.get_text())
                    ab_data['keywords'].add(col_elements[key][1])
                    # Für alle Elemente außer Zeichnungen auch Element-Text zur Keyword-Liste hinzufügen
                    if key != '_zeichnungen':
                        ab_data['keywords'].add(element.get_text())
            # Abfrage für Elemente mit Attribut
            else: 
                for element in ab.find_all(element_from_key):
                    if key == '_messungen' and element.find_parent('table') != (None or ''):
                        continue
                    else:
                        ab_data[key].add(element.get_text())
                        ab_data['keywords'].add(col_elements[key][1])
                        ab_data['keywords'].add(element.get_text())
            # Tabellen gesondert aufbereiten
            for element in ab.find_all('table'):
                measure = '|'
                for cell in element.find_all('cell'):
                    measure = measure + cell.get_text() + '|'
                ab_data['keywords'].add('Messungen')
                ab_data['_messungen'].add(measure)
            ab_line_csv = {key:get_val_string(ab_data[key]) for key in ab_data}
        # Text aufbereiten für Volltext-Spalte (editorische Eingriffe - mit Ausnahme der Ergänzungen - nicht mit aufnehmen; Tabellenzellen abgrenzen, Eingriffe und Besonderheiten ggf. mit Klammern o. markdown formatieren)
        else: 
            for element in ab.find_all('measure'):
                measure = '|'
                for cell in element.find_all('cell'):
                    measure = measure + cell.get_text() + '|'
                element.replace_with(measure)
            for element in ab.find_all('choice'):
                element.replace_with(element.contents[0])
            for element in ab.find_all('supplied'):
                element.insert_before('[')
                element.insert_after(']')
            for element in ab.find_all('del'):
                element.insert_before('~')
                element.insert_after('~')
            for element in ab.find_all('figure'):
                element.insert_before('[Zeichnung: ')
                element.insert_after(']')
            for element in ab.find_all('note'):
                if element.has_attr('resp'):
                    element.decompose()
                else:
                    element.insert_before('{')
                    element.insert_after('}')
            for element in ab.find_all('table'):
                measure = '|'
                for cell in element.find_all('cell'):
                    measure = measure + cell.get_text() + '|'
                element.clear()
                element.append(measure)
            full_text = ab.get_text()
            # Silbentrennung entfernen, da Zeilenumbrüche nicht dem Original entsprechen werden (wegen zeilenweisen Daten in CSV und der Darstellung von Zeilenumbrüchen mit markdown)
            text_column = re.sub('- ','',' '.join(full_text.split()), re.S)
            ab_text = {'_text':text_column}
            ab_line_csv.update(ab_text)
    data.append(ab_line_csv)

data_csv.writerows(data)

data_out.close()