import os
import bs4
import copy

file_dir = os.path.abspath(os.path.dirname(__file__))

data_in = open(os.sep.join([file_dir,'input','manuskripte_tei.xml']),encoding='utf-8',mode='r')
data = data_in.read()
data_in.close()

out_dir = os.sep.join([file_dir,'output'])
os.makedirs(out_dir,exist_ok=True)

data_tree = bs4.BeautifulSoup(data,'xml')

for ab in data_tree.find_all('ab'):
    div = data_tree.new_tag('div')
    head = data_tree.new_tag('head')
    ab.wrap(div)
    title = div.find_previous('pb')['n'].split(sep='_')
    if len(title) == 1:
        pagetype = ''
    else:
        if title[1] == 'r':
            pagetype = 'recto'
        elif title[1] == 'v':
            pagetype = 'verso'
        else:
            pagetype = title[1]
    head.string = 'Seite ' + title[0] + ', ' + pagetype + ' (Bericht ' + ab.find_parent('text')['n'] + ')'
    ab.insert_before(head)



for text in data_tree.group.find_all(lambda x: x.name == 'text' and x.has_attr('n') == True):
    single_report = open(os.sep.join([out_dir,'daten_teipublisher_'+text['n']+'.xml']),encoding='utf-8',mode='w')
    data_tree_part = copy.copy(data_tree)
    data_tree_part.find('text').replace_with(text)
    data_tree_part.find(lambda x: x.name == 'title' and x['xml:lang'] == 'de').string.replace_with(data_tree.find(lambda x: x.name == 'title' and x['xml:lang'] == 'de').string + ' – Bericht Nr. ' + text['n'])
    data_tree_part.find(lambda x: x.name == 'title' and x['xml:lang'] == 'en').string.replace_with(data_tree.find(lambda x: x.name == 'title' and x['xml:lang'] == 'en').string + ' – Report No. ' + text['n'])
    del data_tree_part.find('text')['n']
    del data_tree_part.find('body')['n']
    single_report.write(str(data_tree_part))
    single_report.close()