<?xml version="1.0" encoding="utf-8"?>
<?xml-model href="mfn_manuskripte_schema.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="mfn_manuskripte_odd.xml" type="application/tei+xml" schematypens="http://www.tei-c.org/ns/1.0"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
<fileDesc>
<titleStmt resp="#LK">
<title level="u" xml:id="de_title" xml:lang="de">Exkursionsberichte von Friedrich Sellow (digitale Transkripte)  – Bericht Nr. 19</title>
<title corresp="#de_title" xml:lang="en">Field trip reports written by Friedrich Sellow (digital transcriptions) – Report No. 19</title>
<author xml:id="FS">Friedrich Sellow</author>
<respStmt xml:id="CE_UM">
<name>Carsten Eckert</name>
<name>Ulrich Moritz</name>
<resp>Transkription und Edition</resp>
</respStmt>
<respStmt xml:id="LK">
<name>Laura E. Kaiser</name>
<resp>Konvertierung in TEI-konformes Markup</resp>
</respStmt>
</titleStmt>
<publicationStmt resp="#LK">
<authority>Museum für Naturkunde, Leibniz-Institut für Evolutions- und Biodiversitätsforschung</authority>
<date>2020</date>
<availability status="restricted">
<licence target="https://creativecommons.org/licenses/by-sa/3.0/de/"> 
                    Dieses Werk ist lizenziert unter einer Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Deutschland Lizenz.
                </licence>
</availability>
</publicationStmt>
<sourceDesc>
<bibl>Exkursionsberichte der Reisen durch Brasilien und Uruguay von Friedrich Sellow, 1822-1831</bibl>
</sourceDesc>
</fileDesc>
<encodingDesc resp="#LK">
<projectDesc>
<p>Die Texte wurden von Carsten Eckert und Ulrich Moritz für die wissenschaftliche Nutzung durch die Historische Arbeitsstelle des Museum für Naturkunde, Leibniz Institut für Evolutions- und Biodiversitätsforschung transkribiert.</p>
<p>Im Rahmen einer Masterarbeit wurden die Transkripte aus dem .doc-Format (MS Word) in ein TEI-konformes XML-Dokument überführt, dem das im verknüpften XML Schema spezifizierte Datenmodell zugrunde liegt.</p>
</projectDesc>
<editorialDecl>
<p>Sämtliche editorische Eingriffe wurden in den ursprünglichen Transkripten kenntlich gemacht. Alle dieser Markierungen wurden mit - soweit wie möglich - differenzierenden Tags kenntlich gemacht (siehe die beschriebenen Besonderheiten in &lt;tagsDecl&gt;.</p>
<p>Silbentrennung, Zeilenumbrüche und Interpunktion wurden aus dem Original übernommen.</p>
</editorialDecl>
<tagsDecl partial="true">
<rendition scheme="css" xml:id="et">margin-left:4em;</rendition>
<rendition scheme="css" xml:id="red">color:red</rendition>
<rendition scheme="css" xml:id="r">text-align:right;</rendition>
<rendition scheme="css" xml:id="c">text-align:center;</rendition>
<rendition scheme="css" xml:id="s">text-decoration:line-through</rendition>
<rendition scheme="css" xml:id="sup">vertical-align:super; font-size:.7em</rendition>
<rendition scheme="css" xml:id="u">text-decoration:underline</rendition>
<rendition scheme="css" xml:id="v">float:left; transform:rotate(270deg);</rendition>
<rendition scheme="css" selector="body">text-align:justify;</rendition>
<namespace name="https://tei-c.org/ns/1.0/">
<tagUsage gi="reg" resp="#LK">
                    &lt;reg&gt;-Elemente enthalten vom Original abweichende Schreibweisen. Diese umfassen einerseits, wie entsprechend der TEI Richtlinien für dieses Element vorgesehen, die Normalisierung untypischer Schreibweisen, darüber hinaus jedoch mitunter auch die Korrektur falscher Schreibweisen.
                    Diese Ungenauigkeit ist der ursprünglichen Transkription geschuldet, die keine Kodierung entsprechend der TEI-Richtlinien vorsah und entsprechend keine Differenzierung der editorischen Eingriffe vorgenommen hat. Da die Konvertierung nicht von den Transkribierern selbst, sondern von anderer Hand und weitgehend automatisiert vorgenommen wurde, wurden derartige Korrekturen vereinfachend ebenfalls mittels &lt;reg&gt; annotiert, was gegenüber einer (aufgrund mangelnder Expertise bezüglich historischer Schreibweisen) möglicherweise irrtümlichen Auszeichnung als korrigierte Form (&lt;corr&gt;) als geeignetere Strategie erscheint, die Konformität der Kodierung gegenüber dem TEI Abstract Model zu gewährleisten.
                    Eventuelle spätere Versionen dieser kodierten Texte sollten diesen Mangel beheben, indem Korrekturen innerhalb von &lt;corr&gt;-Elementen stehen und die originale Form entsprechend mit &lt;sic&gt; (anstatt derzeit &lt;orig&gt;) annotiert wird.
                </tagUsage>
<tagUsage gi="supplied" resp="#LK">
                    &lt;supplied&gt;-Elemente kennzeichnen editorische Ergänzungen. Da in den ursprünglichen Transkripten absichtlich abgekürzte Schreibweisen nicht eindeutig von (aufgrund vergessener Zeichen/Wörter) falschen Schreibweisen unterscheidbar waren, wurden jegliche editorischen Einfügungen durch ein &lt;supplied&gt;-Element gekennzeichnet, unabhängig davon, ob sie dem Zweck der Auflösung von Abkürzungen oder der Korrektur von Fehlern durch ausgelassenen Text bzw. Zeichen folgen.
                    In einer künftigen Version sollte diese Ungenauigkeit behoben werden, indem lediglich offensichtlich beabsichtigte Auslassungen mittels des &lt;supplied&gt;-Elements ausgezeichnet werden. Falsche Schreibweisen sollten korrigiert werden, indem die korrekte Schreibweise innerhalb eines &lt;corr&gt;-Elements neben der Original-Variante innerhalb eines &lt;sic&gt;-Elements steht, die wiederum beide von einem &lt;choice&gt;-Element umgeben sind. Abkürzungsauflösungen sollten dann anhand des &lt;expan&gt;-Tags ausgezeichnet werden, der neben dem abgekürzten originalen Textteil als &lt;abbr&gt;-Element innerhalb eines &lt;choice&gt;-Elements steht.
                </tagUsage>
<tagUsage gi="seg" resp="#LK">
                    &lt;seg&gt;-Elemente wurden für die Codierung thematisch kategorisierter Textpassagen verwendet. Darin enthaltene Namen sind aufgrund der automatischen Textcodierung noch nicht als solche codiert worden, weil einerseits nicht zwischen Textpassagen und Namen dieser thematischen Abschnitte vorgenommen wurde und andererseits auch keine automatisierte Zuordnung zum übergeordneten type-Attribut der &lt;name&gt;-Elemente möglich gewesen wäre. Bezüglich der &lt;seg&gt;-Elemente wäre es daher wünschenswert, in einer eventuellen künftigen Version Namen darin separat mit einer entsprechenden type- und subtype-Kategorisierung auszuzeichnen.
                    Darüber hinaus wurden &lt;seg&gt;-Elemente behelfsweise für die Auszeichnung von Messdaten verwendet, weil die fachliche Expertise zur feineren Codierung mittels &lt;measure&gt;- bzw. &lt;measureGrp&gt;-Elementen mit den zugehörigen Attributen fehlte. In künftigen Überarbeitungen sollte auch diese Unschärfe möglichst behoben werden.
                </tagUsage>
<tagUsage gi="foreign">
                    Das &lt;foreign&gt;-Element wird in der derzeitigen Codierung ohne die Angabe der Sprache im xml:lang-Attribut verwendet, weil die Unterscheidung zwischen portugiesisch und spanisch in den Transkripten nicht vorhanden war. In künftigen Überarbeitungen sollte diese Information für &lt;foreign&gt;-Elemente ergänzt werden.
                </tagUsage>
</namespace>
</tagsDecl>
<refsDecl>
<p>Jeder Exkursionsbericht ist über das @n-Attribut eines &lt;text&gt;- wie auch body-Elements eindeutig identifizier- und adressierbar.</p>
<p>Ebenso sind Seitenbeginne innerhalb des @n-Attributs jedes &lt;pb&gt;-Elements nummeriert und identifizierbar.</p>
<p>Darüber hinaus ist jede Seite innerhalb eines &lt;ab&gt;-Element über dessen @xml:id-Attribut eindeutig identifizierbar. Die jeweilige ID folgt dem Muster des regulären Ausdrucks'ZM_S_I_Sellow_Exk_[0-9]{2}_[0-9]{2}a?(_(r|v))?' und stellt die hausinterne Signatur innerhalb des Museums für Naturkunde dar.</p>
</refsDecl>
</encodingDesc>
<profileDesc>
<handNotes>
<handNote corresp="#FS">
<p>Handschrift Friedrich Sellows</p>
</handNote>
<handNote xml:id="CAT">
<p>Handschrift Carl August Tennes</p>
</handNote>
<handNote xml:id="unknown">
<p>Handschrift einer unbekannten Person (bzw. Identifizierung der Handschrift nicht angegeben)</p>
</handNote>
</handNotes>
</profileDesc>
<revisionDesc>
<change when="2020-06-29" who="#LK">
<p>Diese Änderung beinhaltet die Konvertierung der Transkripte aus dem doc-Format in TEI P5 konforme XML-Daten, wobei auch die editorischen Eingriffe der Transkribierer soweit möglich spezifiziert wurden.</p>
</change>
</revisionDesc>
</teiHeader>
<text>
<body>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_1_r__2548.psd" n="01_r" xml:id="ZM_S_I_Sellow_Exk_19_01_r"/>
<div><head>Seite 01, recto (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_01_r">
<lb/> <hi rendition="#c"><hi rendition="#u">Excursion nach der <name type="geographic">Piracicaba</name></hi>
<lb/> <hi rendition="#u">Dienstag den 3 M<supplied reason="omitted-in-original" resp="#CE_UM">ä</supplied>rz.</hi></hi>
<lb/> <hi rendition="#r"><add hand="#unknown"><name type="geographic">Constituição</name>
<lb/> <name type="geographic">Piracicaba</name></add></hi>
<lb/> Keine Gegend in der <name type="geographic">Provinz S. Paolo</name> hat sich bisher so 
<lb/> vortheilhaft für die <seg type="agriculture_crafts">Kultur des Zuckerrohrs</seg> bewährt, 
<lb/> als die Districte von <name type="geographic">Piracicaba</name> oder <name type="geographic">V<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">il</supplied>a</hi> do Consti</name>- 
<lb/> <name type="geographic">tuição</name> und von <name type="geographic">Campinas</name> oder <name type="geographic">V<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">il</supplied>a</hi> de S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Carlos</name>, und 
<lb/> der rothbraune <name type="rock_mineral">Lehmen (Barro vermelho)</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welcher dort 
<lb/> das reichste Schilf hervorbringt<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> ist in guten Ruf 
<lb/> gekommen. Eine der reichsten Gutsbesitzer in diesen Distric- 
<lb/> ten, der D<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">o</supplied>n</hi> <name type="person">José do Costa Carvalho</name>, der eine flüchtige Reise nach seinen 
<lb/> dortigen 7 <name type="object">Engenhos</name> <note resp="#CE_UM" type="editorial">Zuckerrohrmühlen</note> machen wollte, lud mich dahin ein, 
<lb/> und besonders gern benutzte ich die Gelegenheit<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> eine 
<lb/> der wichtigsten Theile der Provinz kennen zu lernen, und 
<lb/> einige Tage in der Gesellschaft eines Mannes zuzubringen 
<lb/> von vielfältiger Bildung, der zu denen gehört, die um die 
<lb/> Freiheit Brasiliens am Meisten Verdienst haben. Der D<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">o</supplied>n</hi>
<lb/> <name type="person">Juão Baptista Badaro</name>, ein guter Botaniker, schloß sich auch an<supplied reason="omitted-in-original" resp="#CE_UM">.</supplied>
<lb/> Wir verließen die Stadt gegen zwei Uhr, in der Absicht<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> heute bis zum <name type="geographic">Passo do </name><name type="person">Juquery</name> zu gehen. Unser Weg 
<lb/> war über die <name type="geographic">Ponta de Lorena</name> durch die Vorstadt von <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">anta</supplied> Ephi</name>- 
<lb/> <name type="geographic">genia</name>, über <name type="geographic">Agua branca</name> nach der <name type="geographic">Ponta do Anastasio</name>. 
<lb/> Wenn man die langen Erdmauern<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> die meist ein ruinen- 
<lb/> ähnliches Ansehen haben<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> jener Vorstadt im Rücken hat, 
<lb/> wird die Landschaft unterhaltend, ja anmuthig. Der Weg<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> der sehr gut ist<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> läuft durch Campo<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welches feine Gebüsche hat. 
<lb/> Auf einer großen Krümmung gen West nähert man sich 
<lb/> dem in St<supplied reason="omitted-in-original" resp="#CE_UM">unde</supplied> 5 streichenden Gebirgszuge<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welcher die 
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_1_v__2549.tif" n="01_v" xml:id="ZM_S_I_Sellow_Exk_19_01_v"/>
<div><head>Seite 01, verso (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_01_v">
<lb/> <name type="geographic">Hochebene von S. Paolo, die Campos de Piratininga</name>, gen Nord 
<lb/> begränzt, und wovon der <name type="geographic">Moro de Jaraguá</name>, der sich durch 
<lb/> eine spitze Nase auszeichnet, ein Theil ist. Ein Zug hoher 
<lb/> Hügel<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welcher parallel mit dem Gebirge sich am 
<lb/> rechten Ufer des <name type="geographic"><choice><orig>Tieté </orig><reg resp="#CE_UM">Rio Tietê</reg></choice></name> hinzieht<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> bildet einen Übergang 
<lb/> aus dem flachen Lande ins Gebirge, und leiht der 
<lb/> Gegend einen sanften Charakter. Die Hügel oder 
<lb/> Vorberge sind noch mit <name type="object">Campo</name> größtentheils bedeckt, 
<lb/> das Gebirge hingegen mit Waldung. Einige Stellen 
<lb/> des Weges bieten vortreffliche Ansichten dar, wie der 
<lb/> Punkt Alto do <name type="geographic"><choice><orig>Paquembú </orig><reg resp="#CE_UM">Pacaembu</reg></choice></name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wo man<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> den Stufen des Gebir- 
<lb/> ges schon genähert, einen weiten Bogen des hier schon beträcht- 
<lb/> lichen <name type="geographic">Tieté</name> vor sich hat. <name type="geographic">Agua branca</name> ist ein kleiner heller 
<lb/> Bach<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> an welchem ein <name type="object">Rancho</name> und eine <name type="object">Venda</name>. Man rechnet 
<lb/> 3 L<supplied reason="omitted-in-original" resp="#CE_UM">egoa</supplied> von der Stadt bis zum <supplied reason="omitted-in-original" resp="#CE_UM">Rio</supplied> <name type="geographic">Jaraguá</name> nämlich<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> zwei bis 
<lb/> zur <name type="geographic">Ponta do Anastasio</name> und von hier 1 L<supplied reason="omitted-in-original" resp="#CE_UM">egoa</supplied> bis zum Berge. 
<lb/> Diese Brücke ist von Holz<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> 60 Schritt lang und war in 
<lb/> gutem Zustand. Der Fluß geht hier rasch zwischen schattigen 
<lb/> Ufern hinab. Das erste Gestein<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welches wir an diesem 
<lb/> Gebirge zu sehen bekamen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> war <name type="rock_mineral">Granit</name> mit Neigung ins 
<lb/> <name type="rock_mineral">Sphäroide</name> abzuwittern, dann folgte <name type="rock_mineral">Glimmerschiefer</name>, genau 
<lb/> beschaffen wie 1703 und 1704. Später wieder <name type="rock_mineral">Granit</name> und 
<lb/> so abwechselnd. Die <name type="geographic">Fazenda de Jaraguá</name> bekamen 
<lb/> wir nicht zu sehen, sie blieb uns verdeckt, da sie tief 
<lb/> hart am südöstl<supplied reason="omitted-in-original" resp="#CE_UM">ichen</supplied> Fuße des Berges liegt, der uns zur Lin- 
<lb/> ken blieb. Der Abhang des Gebirges ist nicht sonderlich 
<lb/> steil sowenig als die der anderen <name type="object">Joche</name> zwischen <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied></name>
<lb/> <name type="geographic">Paulo</name> und <name type="geographic"><choice><orig>Jundiahy </orig><reg resp="#CE_UM">Jundiaí</reg></choice></name> und in der That pflegten früher 
<lb/> Frachtwagen von dem <name type="geographic">Districte da Franca</name> bis 
<lb/> zur Stadt zu kommen. Da fiel aber dem Magistrate 
<lb/> ein<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> von jedem beladenen Karren eine Abgabe von 10,000 reis 
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_2_r__2550.tif" n="02_r" xml:id="ZM_S_I_Sellow_Exk_19_02_r"/>
<div><head>Seite 02, recto (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_02_r">
<lb/> reis zu fordern, während er um das Ausbessern der Wege 
<lb/> sich nie bekümmerte. Die Folge war<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> daß sie wegblieben, nur 
<lb/> bis <name type="geographic">Campinas</name> gingen, und dort sich ihre Bedürfnisse zu ver- 
<lb/> schaffen suchten, wodurch der Wohlstand dieses Orts nicht wenig 
<lb/> befördert wurde. Die Höhe<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> zu welcher die Straße hier an- 
<lb/> steigt<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> ist unbedeutend. Bald nachdem man den höchsten 
<lb/> Punkt der Straße zwischen <name type="geographic"> <choice><orig>Juquery </orig><reg resp="#CE_UM">Juqueri</reg></choice></name> im Rücken 
<lb/> hat<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> findet man einen Rancho, R<supplied reason="omitted-in-original" resp="#CE_UM">ancho</supplied> grande genannt. 
<lb/> Dergleichen finden sich auf der ganzen Straße nach <name type="geographic">Santos</name>
<lb/> zwei bis 3 L<supplied reason="omitted-in-original" resp="#CE_UM">egoa</supplied> von einander entfernt. Sie sind mit 
<lb/> Ziegel gedeckt, und haben <name type="object">Taipamauern</name> <note resp="#CE_UM" type="editorial">Lehmmauern</note><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> wodurch der <name type="ethnicity_group">Tropeiro</name> besser gegen Winde und 
<lb/> um des Salzes wegen eindringendes Vieh geschützt ist. 
<lb/> Zwei gegenüber stehende Thürme gestatten Ein und Ausgang. 
<lb/> Der <name type="geographic">Jaraguá</name> ist auf der nördl<supplied reason="omitted-in-original" resp="#CE_UM">ichen</supplied> Seite mit Campo bewachsen. 
<lb/> Andere diesseitige Abhänge des eben überstiegenen Gebirgs- 
<lb/> zuges, welche Waldung hervorbringen, zeigten sich bis zu einer 
<lb/> beträchtlichen Höhe angebaut, mit <seg type="agriculture_crafts">Mais</seg> und <seg type="agriculture_crafts">Bohnen</seg><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> welche gut dort gedeihen sollen. Auch ein <seg type="agriculture_crafts">Zuckerfeld</seg> sah ich 
<lb/> auf einer Höhe<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche wohl nicht unter 3200 Fuß engl<supplied reason="omitted-in-original" resp="#CE_UM">isch</supplied> über 
<lb/> der Meeresfläche lag, welches beweist<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wieviel auf eine 
<lb/> Lage ankommt<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wo dergleichen zahrte Pflanzen wider 
<lb/> Süd-Ost und Ostwinde geschützt, und wo die ganze Kraft 
<lb/> der Sonne ungehindert einwirken kann. Im Ganzen erscheinen 
<lb/> aber nur äußerst wenig angebaute Stellen. Wir erreichten 
<lb/> bei Sonnenuntergang den <name type="geographic"><choice><orig> Rio de Juquiry</orig> <reg resp="#CE_UM">Rio Juqueri</reg></choice></name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> der hier mit 20 
<lb/> Schritt Breite in der 5<hi rendition="#sup">ten</hi> Stunde hinabgeht und eine hölzer- 
<lb/> ne Brücke hat. Am linken Ufer steht ein <name type="object">Venda</name>, wo 
<lb/> wir Nachtlager nahmen. Der Wirth klagte<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> daß sein ganzes 
<lb/> Haus mit Krankheit behaftet sei, daß er nur wenig Bequemlich- 
<lb/> keit geben könne, doch was er besäße<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> sollte für uns heraus. Er hatte gut Anbieten; er wußte wohl<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> daß <name type="person">Costa</name> einen 
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_2_v__2551.tif" n="02_v" xml:id="ZM_S_I_Sellow_Exk_19_02_v"/>
<div><head>Seite 02, verso (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_02_v">
<lb/> Überfluß an guten Provisionen und ein Feldbett mitzuführen 
<lb/> pflegte. <seg type="agriculture_crafts">Mais für die Thiere</seg> war das Einzige<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> was wir von 
<lb/> ihm bedurften. Während des Abendessens stellte er sich ein 
<lb/> und ließ sich ein Glas Wein wohl schmecken. 
<lb/> <hi rendition="#c">Mittwoch den 4ten M<supplied reason="omitted-in-original" resp="#CE_UM">ä</supplied>rz</hi>
<lb/> Bei Sonnenaufgang stand an der Brücke des <name type="geographic">Juquiry</name>
<lb/> <hi rendition="#et">
<table cols="10" rows="2">
<row>
<cell> <seg type="measurement-data">a</seg> </cell> <cell> <seg type="measurement-data">c</seg> </cell> <cell> <seg type="measurement-data">g</seg> </cell> <cell> <seg type="measurement-data">26</seg> </cell> <cell> <seg type="measurement-data">1</seg> </cell> <cell> <seg type="measurement-data">06</seg> </cell> <cell> <seg type="measurement-data">29</seg> </cell> <cell> <seg type="measurement-data">3</seg> </cell> <cell> <seg type="measurement-data">86</seg> </cell> <cell> <seg type="measurement-data">SO</seg> </cell>
</row>
<row>
<cell> <seg type="measurement-data">15</seg> </cell> <cell> <seg type="measurement-data">16</seg> </cell> <cell> <seg type="measurement-data">15</seg> </cell> <cell> </cell> <cell> </cell> <cell> </cell> <cell> </cell> <cell> </cell> <cell> </cell> <cell> </cell>
</row>
</table> </hi>
<lb/> Einige Kinder belustigten sich mit dem Safte der <name type="plant">Jatropha 
<lb/> Curcas</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welcher sehr reichlich hervorquillt, sobald man 
<lb/> einen jungen Zweig durchbricht, und vermittelst einer klei- 
<lb/> nen Ruthe<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> die zu einer Schlinge umgebogen war<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> Blasen zu werfen, welche meist sehr groß und schön ausfielen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> so schön<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> als man sie <supplied reason="omitted-in-original" resp="#CE_UM">sich</supplied> aus Seifenwasser nur vorstellen kann. 
<lb/> Dicht bei der <name type="object">Venda</name> steht <name type="rock_mineral">Glimmerschiefer</name> zu Tage<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> in St<supplied reason="omitted-in-original" resp="#CE_UM">unde</supplied>
<lb/> 4 4 und 5 streichend<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> mit weißem Glimmer, sehr in Ver- 
<lb/> witterung und <name type="rock_mineral">eisenschüssig</name>. Hier gesellte sich ein reicher 
<lb/> Gutsbesitzer aus <name type="geographic">Piracicaba</name>, der Adjudant <name type="person">Albaro</name>, zu 
<lb/> uns, der dem <name type="person">D<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">o</supplied>n</hi> Costa</name> entgegen kam und sofort das 
<lb/> Amt eines <name type="ethnicity_group">Vaqueiro</name> übernahm. Wir kamen gegen 7 Uhr 
<lb/> weiter und erstiegen bald die <name type="geographic">S<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">err</supplied>a</hi> do <choice><orig/><reg resp="#CE_UM">Juqueri</reg></choice></name>, einen dem 
<lb/> gestern zurückgelassenen Gebirge paralleler Zug, der aber 
<lb/> lange nicht die Höhe des <name type="geographic">Jaraguá</name> erreicht. Er besteht 
<lb/> aus <name type="rock_mineral">Granit</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> in welchem mächtige <name type="rock_mineral">Quarzgänge</name>. Der Granit 
<lb/> wechselt mit <name type="rock_mineral">Glimmerschiefer</name> ab und auf diesem liegt <name type="rock_mineral">Thon- 
<lb/> schiefer</name> (1713)<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welcher die Hauptmasse eines and<supplied reason="omitted-in-original" resp="#CE_UM">eren</supplied>
<lb/> parallelen Rückens, die <name type="geographic">Serra do Monjolinho</name>, bildet, 
<lb/> die wir nun zunächst überstiegen. 
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_3_r__2552.tif" n="03_r" xml:id="ZM_S_I_Sellow_Exk_19_03_r"/>
<div><head>Seite 03, recto (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_03_r">
<lb/> Von dieser Gegend blickt man auf die Gegend von <name type="geographic"><choice><orig>Jundiahy </orig><reg resp="#CE_UM">Jundiaí</reg></choice></name>
<lb/> hinab und bekommt die <name type="geographic">Serra do Japi</name> zu sehen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche ein 
<lb/> anderes Streichen als die erwähnten Joche hat, nämlich in 
<lb/> der 1<hi rendition="#sup">sten</hi> Stunde und an welcher der <name type="geographic">Rio do Jundiahy</name> hin- 
<lb/> geht. Indessen zeigt sich doch noch ein jenen paralle- 
<lb/> ler Zug in beträchtlicher Entfernung<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> aber noch diesseits 
<lb/> <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Carlos</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> über welchem hinaus der <name type="rock_mineral">Flötzboden</name> beginnt. 
<lb/> Mehrere Male kamen Abhänge zu Gesicht, welche mit Hochwald 
<lb/> bekleidet waren und deren Schönheit von <name type="person">Badaro</name> und mir 
<lb/> gelobt wurde; allein <name type="person">Albaro</name> sprach nur darüber mit Ver- 
<lb/> achtung, als einem unfruchtbaren Boden, der keinen <seg type="agriculture_crafts">Mais</seg>
<lb/> hervorbringen würde, was man aus der <name type="plant">Barba do pau (Tilland- 
<lb/> sia usneoides)</name> schließen könne<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> mit der die alten Bäume be- 
<lb/> deckt wären. Es ist wahr<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> daß im Allgemeinen in diesem 
<lb/> Gebirge die Bäume einen krüpplichen Wuchs haben; auch sieht 
<lb/> man hier fast gar keine Palmen, nur selten einige Jiri-
<lb/> bapalmen; doch ließe sich gewiß manche fruchtbare Stelle 
<lb/> hier auswählen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> die ihren Bearbeiter nähren würde. 
<lb/> <name type="person">Albaro</name> wollte nichts für gut anerkennen als seinen 
<lb/> <name type="object">Barro rocha</name> <note resp="#CE_UM" type="editorial">Lehmboden</note>. <name type="plant">Barba de Pau</name> scheint vorzüglich feuchte 
<lb/> Gegenden zu lieben wie Gebirgsgipfel und Flußmündungen. 
<lb/> In Letzteren ist sie auf den alten Feigenbäumen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche 
<lb/> hier gedeihen, oft ungemein häufig, und doch sind diese 
<lb/> Niederungen von großer Fruchtbarkeit, namentlich an 
<lb/> vielen Küstenflüssen. Der Tag wurde heiß; wir kehrten 
<lb/> gegen 11 Uhr, um Mittagsruhe zu halten<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> bei einem <name type="ethnicity_group">Fazendeiro</name> <note resp="#CE_UM" type="editorial">Landwirt</note>
<lb/> Namens <name type="person">Affanço</name> ein, 1 L<supplied reason="omitted-in-original" resp="#CE_UM">egoa</supplied> von Jundiahy. 
<lb/> <hi rendition="#r">hier</hi>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_3_v__2553.tif" n="03_v" xml:id="ZM_S_I_Sellow_Exk_19_03_v"/>
<div><head>Seite 03, verso (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_03_v">
<lb/> hier stand um Mittag 
<lb/>
<table cols="5" rows="2">
<row>
<cell> <seg type="measurement-data">a</seg> </cell> <cell> <seg type="measurement-data">c</seg> </cell> <cell> <seg type="measurement-data">g</seg> </cell> <cell> </cell> <cell> </cell>
</row>
<row>
<cell> <seg type="measurement-data">20</seg> </cell> <cell> <seg type="measurement-data">21,4</seg> </cell> <cell> </cell> <cell> <seg type="measurement-data">25 11 63</seg> </cell> <cell> <seg type="measurement-data">29 3 40 W NW Wolkig</seg> </cell>
</row>
</table>
<lb/> Gegen 3 gingen wir weiter und waren gegen 4 Uhr in <name type="geographic">Jun-</name>
<lb/> <name type="geographic">diahy</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wo wir neben der <name type="object">Matriz</name> im Hause des <name type="person">Juiz de</name>
<lb/> <name type="person">Paz</name> einen Augenblick abstiegen. <name type="geographic">Jundiahy</name> liegt nahe am 
<lb/> Gebirge<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welches wir nun zurückließen, auf einem flachen 
<lb/> Hügel, der ein günstiges Terrain für eine Ortschaft abgiebt. 
<lb/> Es ist regelmäßig gebaut, grade sich in rechten Winkeln 
<lb/> kreuzende Straßen, ein großer Platz vor der Kirche. 
<lb/> Die Gebäude einst nur <name type="object">Taipas</name> <note resp="#CE_UM" type="editorial">Lehmhütten</note>, wozu der Boden sich hier 
<lb/> so gut als in <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo</name> zu schicken scheint. Die Einwohner trei-
<lb/> ben hier <seg type="agriculture_crafts">Viehzucht</seg>, <seg type="agriculture_crafts">Handel mit Vieh</seg> und etwas <seg type="agriculture_crafts">Ackerbau</seg>; mehrere 
<lb/> fabrizieren <seg type="agriculture_crafts">Zucker</seg>. Das Terrain steigt gen NW hin etwas 
<lb/> an und von dort aus, in der Nähe einer Capelle am 
<lb/> Ausgange aus der V<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">il</supplied>a</hi> hat <name type="geographic">Jundiahy</name> eine interessante 
<lb/> Ansicht bei hellem Wetter<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wo das Gebirge zwischen 
<lb/> hier <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo</name> den Hintergrund der Landschaft bildet. 
<lb/> Die Gegend um die V<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">il</supplied>a</hi> ist buschiges <name type="object">Campo</name>; allein dieses 
<lb/> gleicht mehr dem von <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo</name> als dem von <name type="geographic">Jaracaba</name>. 
<lb/> Um 4 Uhr stand hier 
<lb/>
<table cols="4" rows="2">
<row>
<cell> <seg type="measurement-data">a c</seg> </cell> <cell> <seg type="measurement-data">g</seg> </cell> <cell> </cell> <cell> </cell>
</row>
<row>
<cell> <seg type="measurement-data">22</seg> </cell> <cell> <seg type="measurement-data">23</seg> </cell> <cell> <seg type="measurement-data">25 11 20</seg> </cell> <cell> <seg type="measurement-data">29 3 14. NW wenige Wolken.</seg> </cell>
</row>
</table>
<lb/> Das Klima soll hier etwas milder sein als zu <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo</name><supplied reason="omitted-in-original" resp="#CE_UM">.</supplied>
<lb/> Die Nebel<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche im Thal von S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo zu häufig vor- 
<lb/> kommen bei S.O Winden, <supplied reason="omitted-in-original" resp="#CE_UM">bleiben</supplied> hier nicht so lange liegen. 
<lb/> Man rechnet von hier zur Stadt 9 Legoas oder 9 ½<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> nach <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied></name>
<lb/> <name type="geographic">João do Tibaia</name> 7, nach <name type="geographic"><choice><orig>Ytú </orig><reg resp="#CE_UM">Itu</reg></choice> </name>9, nach <name type="geographic">Jaracabu</name> 15, nach <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Carlos</name>
<lb/> 7, nach <name type="geographic">Piracicaba</name> 15. 
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_19_4_r__2554.tif" n="04_r" xml:id="ZM_S_I_Sellow_Exk_19_04_r"/>
<div><head>Seite 04, recto (Bericht 19)</head><ab corresp="#ZM_S_I_Sellow_Exk_19_04_r">
<lb/> Wir machten auf dem geraden Wege von hier nach <name type="geographic">Piracicaba</name>
<lb/> heute noch 3 ½ L<supplied reason="omitted-in-original" resp="#CE_UM">egoas</supplied> bis zur F<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">azen</supplied>da</hi> des <name type="ethnicity_group">Sarg<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">en</supplied>to</hi> Mor</name> <name type="person">Querez (Ant<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">oni</supplied>o</hi>
<lb/> Querez Telles)</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> wo <seg type="agriculture_crafts">Zuckerbau</seg> Hauptgeschäft ist. <name type="person">Querez</name> wie sein Stief-Vater 
<lb/> waren in <name type="geographic"> <choice><orig/><reg resp="#CE_UM"> Goiás</reg></choice> </name>und <name type="geographic"><choice><orig>Cuijabá</orig> <reg resp="#CE_UM">Cuiabá</reg></choice></name> gewesen und lobten die Provinzen, 
<lb/> besonders von letzterer wußten sie viel gutes zu sagen. 
<lb/> <hi rendition="#et">Die <seg type="agriculture_crafts">Zuckermühle</seg> stand hier dicht am Wohnhause, etwas niedriger,</hi>
<lb/> so daß man aus einem Zimmer des Ersteren auf die <name type="object">Moenda</name> <note resp="#CE_UM" type="editorial">Mühle</note>
<lb/> hinabsehen konnte, und auf einer Treppe von dort aus hinabsteigen<supplied reason="omitted-in-original" resp="#CE_UM">.</supplied>
<lb/> Diese Einrichtung gewährt seinen Vortheil, nur sind der immer 
<lb/> währende Lärm während der Erndtezeit, und die viele Fliegen 
<lb/> Mücken<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche der Zuckersaft alsdann stets herbei lockt auch 
<lb/> beträchtliche Nachtheile. Ein beträchtliches Stück künstliches <name type="object">Campo</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> mit 
<lb/> <seg type="agriculture_crafts"> <choice><orig/><reg resp="#CE_UM">Grama</reg></choice></seg> bepflanzt<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> war vor der <name type="object">Faz<supplied reason="omitted-in-original" resp="#CE_UM">en</supplied><hi rendition="#sup">da</hi></name>. Dieses ist sehr wichtig 
<lb/> zur Ernährung der Ochsen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche zum Malen des Schilfs, da man 
<lb/> nicht vermittelst Wasser die Mühle in Bewegung setzt, nöthig hat. 
<lb/> Der Weg von <name type="geographic">Jundiahy</name> hierher ist im Ganzen sanfter 
<lb/> als der jenseits dieses Orts; nur in der Nähe dieser <name type="object">Faz<supplied reason="omitted-in-original" resp="#CE_UM">en</supplied><hi rendition="#sup">da</hi></name>
<lb/> übersteigt man einige nicht geringe Hügel<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche man 
<lb/> mit schweren Lasten nicht wohl hinauf fahren könnte. Das Terrain 
<lb/> ist noch Urgebirge und die Vegetation noch beinahe die der Gegend 
<lb/> von <name type="geographic">S<supplied reason="omitted-in-original" resp="#CE_UM">ão</supplied> Paulo</name>. Bis zum <name type="geographic">Rio do Jundiahy</name>, ½ L<supplied reason="omitted-in-original" resp="#CE_UM">egoa</supplied> von der V<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">il</supplied>a</hi><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied>
<lb/> mit 40 Sch<supplied reason="omitted-in-original" resp="#CE_UM">ritt</supplied> Breite in der 4<hi rendition="#sup">ten</hi> St<supplied reason="omitted-in-original" resp="#CE_UM">unde</supplied> hier hingehend<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> läuft der 
<lb/> Weg durch Campo. Nachdem man diesen zurückgelassen<supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> gewinnt 
<lb/> man von einigen Anhöhen aus noch eine freie Aussicht 
<lb/> auf die <name type="geographic">S<hi rendition="#sup"><supplied reason="omitted-in-original" resp="#CE_UM">err</supplied>a</hi> do Japi</name><supplied reason="omitted-in-original" resp="#CE_UM">,</supplied> welche unter verschiedenen Namen 
<lb/> bis <name type="geographic">Ytú</name> <supplied reason="omitted-in-original" resp="#CE_UM">und</supplied> <name type="geographic">Sorocaba</name> sich fortsetzt und an der der <name type="geographic">Jundi</name>-
<lb/> <name type="geographic">ahy</name> hingleitet bis er oberhalb des Salto in den <name type="geographic">Tieté</name> fällt. 
<lb/> <hi rendition="#c">Donnerstag den 5<hi rendition="#sup">ten</hi> M<supplied reason="omitted-in-original" resp="#CE_UM">ä</supplied>rz</hi>
<lb/> Der <name type="ethnicity_group">Neger</name> <name type="person">Querez</name> begleitete uns diesen Morgen bis zum 
<lb/> <name type="geographic"><choice><orig> Capivary mirim</orig> <reg resp="#CE_UM">Rio Capivari-Mirim</reg></choice>.</name>
</ab></div>
</body>
</text>
</TEI>