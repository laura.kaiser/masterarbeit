<?xml version="1.0" encoding="utf-8"?>
<?xml-model href="mfn_manuskripte_schema.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="mfn_manuskripte_odd.xml" type="application/tei+xml" schematypens="http://www.tei-c.org/ns/1.0"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
<fileDesc>
<titleStmt resp="#LK">
<title level="u" xml:id="de_title" xml:lang="de">Exkursionsberichte von Friedrich Sellow (digitale Transkripte)  – Bericht Nr. 16</title>
<title corresp="#de_title" xml:lang="en">Field trip reports written by Friedrich Sellow (digital transcriptions) – Report No. 16</title>
<author xml:id="FS">Friedrich Sellow</author>
<respStmt xml:id="CE_UM">
<name>Carsten Eckert</name>
<name>Ulrich Moritz</name>
<resp>Transkription und Edition</resp>
</respStmt>
<respStmt xml:id="LK">
<name>Laura E. Kaiser</name>
<resp>Konvertierung in TEI-konformes Markup</resp>
</respStmt>
</titleStmt>
<publicationStmt resp="#LK">
<authority>Museum für Naturkunde, Leibniz-Institut für Evolutions- und Biodiversitätsforschung</authority>
<date>2020</date>
<availability status="restricted">
<licence target="https://creativecommons.org/licenses/by-sa/3.0/de/"> 
                    Dieses Werk ist lizenziert unter einer Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Deutschland Lizenz.
                </licence>
</availability>
</publicationStmt>
<sourceDesc>
<bibl>Exkursionsberichte der Reisen durch Brasilien und Uruguay von Friedrich Sellow, 1822-1831</bibl>
</sourceDesc>
</fileDesc>
<encodingDesc resp="#LK">
<projectDesc>
<p>Die Texte wurden von Carsten Eckert und Ulrich Moritz für die wissenschaftliche Nutzung durch die Historische Arbeitsstelle des Museum für Naturkunde, Leibniz Institut für Evolutions- und Biodiversitätsforschung transkribiert.</p>
<p>Im Rahmen einer Masterarbeit wurden die Transkripte aus dem .doc-Format (MS Word) in ein TEI-konformes XML-Dokument überführt, dem das im verknüpften XML Schema spezifizierte Datenmodell zugrunde liegt.</p>
</projectDesc>
<editorialDecl>
<p>Sämtliche editorische Eingriffe wurden in den ursprünglichen Transkripten kenntlich gemacht. Alle dieser Markierungen wurden mit - soweit wie möglich - differenzierenden Tags kenntlich gemacht (siehe die beschriebenen Besonderheiten in &lt;tagsDecl&gt;.</p>
<p>Silbentrennung, Zeilenumbrüche und Interpunktion wurden aus dem Original übernommen.</p>
</editorialDecl>
<tagsDecl partial="true">
<rendition scheme="css" xml:id="et">margin-left:4em;</rendition>
<rendition scheme="css" xml:id="red">color:red</rendition>
<rendition scheme="css" xml:id="r">text-align:right;</rendition>
<rendition scheme="css" xml:id="c">text-align:center;</rendition>
<rendition scheme="css" xml:id="s">text-decoration:line-through</rendition>
<rendition scheme="css" xml:id="sup">vertical-align:super; font-size:.7em</rendition>
<rendition scheme="css" xml:id="u">text-decoration:underline</rendition>
<rendition scheme="css" xml:id="v">float:left; transform:rotate(270deg);</rendition>
<rendition scheme="css" selector="body">text-align:justify;</rendition>
<namespace name="https://tei-c.org/ns/1.0/">
<tagUsage gi="reg" resp="#LK">
                    &lt;reg&gt;-Elemente enthalten vom Original abweichende Schreibweisen. Diese umfassen einerseits, wie entsprechend der TEI Richtlinien für dieses Element vorgesehen, die Normalisierung untypischer Schreibweisen, darüber hinaus jedoch mitunter auch die Korrektur falscher Schreibweisen.
                    Diese Ungenauigkeit ist der ursprünglichen Transkription geschuldet, die keine Kodierung entsprechend der TEI-Richtlinien vorsah und entsprechend keine Differenzierung der editorischen Eingriffe vorgenommen hat. Da die Konvertierung nicht von den Transkribierern selbst, sondern von anderer Hand und weitgehend automatisiert vorgenommen wurde, wurden derartige Korrekturen vereinfachend ebenfalls mittels &lt;reg&gt; annotiert, was gegenüber einer (aufgrund mangelnder Expertise bezüglich historischer Schreibweisen) möglicherweise irrtümlichen Auszeichnung als korrigierte Form (&lt;corr&gt;) als geeignetere Strategie erscheint, die Konformität der Kodierung gegenüber dem TEI Abstract Model zu gewährleisten.
                    Eventuelle spätere Versionen dieser kodierten Texte sollten diesen Mangel beheben, indem Korrekturen innerhalb von &lt;corr&gt;-Elementen stehen und die originale Form entsprechend mit &lt;sic&gt; (anstatt derzeit &lt;orig&gt;) annotiert wird.
                </tagUsage>
<tagUsage gi="supplied" resp="#LK">
                    &lt;supplied&gt;-Elemente kennzeichnen editorische Ergänzungen. Da in den ursprünglichen Transkripten absichtlich abgekürzte Schreibweisen nicht eindeutig von (aufgrund vergessener Zeichen/Wörter) falschen Schreibweisen unterscheidbar waren, wurden jegliche editorischen Einfügungen durch ein &lt;supplied&gt;-Element gekennzeichnet, unabhängig davon, ob sie dem Zweck der Auflösung von Abkürzungen oder der Korrektur von Fehlern durch ausgelassenen Text bzw. Zeichen folgen.
                    In einer künftigen Version sollte diese Ungenauigkeit behoben werden, indem lediglich offensichtlich beabsichtigte Auslassungen mittels des &lt;supplied&gt;-Elements ausgezeichnet werden. Falsche Schreibweisen sollten korrigiert werden, indem die korrekte Schreibweise innerhalb eines &lt;corr&gt;-Elements neben der Original-Variante innerhalb eines &lt;sic&gt;-Elements steht, die wiederum beide von einem &lt;choice&gt;-Element umgeben sind. Abkürzungsauflösungen sollten dann anhand des &lt;expan&gt;-Tags ausgezeichnet werden, der neben dem abgekürzten originalen Textteil als &lt;abbr&gt;-Element innerhalb eines &lt;choice&gt;-Elements steht.
                </tagUsage>
<tagUsage gi="seg" resp="#LK">
                    &lt;seg&gt;-Elemente wurden für die Codierung thematisch kategorisierter Textpassagen verwendet. Darin enthaltene Namen sind aufgrund der automatischen Textcodierung noch nicht als solche codiert worden, weil einerseits nicht zwischen Textpassagen und Namen dieser thematischen Abschnitte vorgenommen wurde und andererseits auch keine automatisierte Zuordnung zum übergeordneten type-Attribut der &lt;name&gt;-Elemente möglich gewesen wäre. Bezüglich der &lt;seg&gt;-Elemente wäre es daher wünschenswert, in einer eventuellen künftigen Version Namen darin separat mit einer entsprechenden type- und subtype-Kategorisierung auszuzeichnen.
                    Darüber hinaus wurden &lt;seg&gt;-Elemente behelfsweise für die Auszeichnung von Messdaten verwendet, weil die fachliche Expertise zur feineren Codierung mittels &lt;measure&gt;- bzw. &lt;measureGrp&gt;-Elementen mit den zugehörigen Attributen fehlte. In künftigen Überarbeitungen sollte auch diese Unschärfe möglichst behoben werden.
                </tagUsage>
<tagUsage gi="foreign">
                    Das &lt;foreign&gt;-Element wird in der derzeitigen Codierung ohne die Angabe der Sprache im xml:lang-Attribut verwendet, weil die Unterscheidung zwischen portugiesisch und spanisch in den Transkripten nicht vorhanden war. In künftigen Überarbeitungen sollte diese Information für &lt;foreign&gt;-Elemente ergänzt werden.
                </tagUsage>
</namespace>
</tagsDecl>
<refsDecl>
<p>Jeder Exkursionsbericht ist über das @n-Attribut eines &lt;text&gt;- wie auch body-Elements eindeutig identifizier- und adressierbar.</p>
<p>Ebenso sind Seitenbeginne innerhalb des @n-Attributs jedes &lt;pb&gt;-Elements nummeriert und identifizierbar.</p>
<p>Darüber hinaus ist jede Seite innerhalb eines &lt;ab&gt;-Element über dessen @xml:id-Attribut eindeutig identifizierbar. Die jeweilige ID folgt dem Muster des regulären Ausdrucks'ZM_S_I_Sellow_Exk_[0-9]{2}_[0-9]{2}a?(_(r|v))?' und stellt die hausinterne Signatur innerhalb des Museums für Naturkunde dar.</p>
</refsDecl>
</encodingDesc>
<profileDesc>
<handNotes>
<handNote corresp="#FS">
<p>Handschrift Friedrich Sellows</p>
</handNote>
<handNote xml:id="CAT">
<p>Handschrift Carl August Tennes</p>
</handNote>
<handNote xml:id="unknown">
<p>Handschrift einer unbekannten Person (bzw. Identifizierung der Handschrift nicht angegeben)</p>
</handNote>
</handNotes>
</profileDesc>
<revisionDesc>
<change when="2020-06-29" who="#LK">
<p>Diese Änderung beinhaltet die Konvertierung der Transkripte aus dem doc-Format in TEI P5 konforme XML-Daten, wobei auch die editorischen Eingriffe der Transkribierer soweit möglich spezifiziert wurden.</p>
</change>
</revisionDesc>
</teiHeader>
<text>
<body>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_1_r__2519.tif" n="01_r" xml:id="ZM_S_I_Sellow_Exk_16_01_r"/>
<div><head>Seite 01, recto (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_01_r"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v:</note> <foreign xml:lang="pt">Expedição de Guarapuava Ant.o (Antonio)ranca e Horta, do meu Conselho, Governador e Capitão General da Capa(Capitania) Amo(amigo). Eu o Principe Regente vos envio muito saudar. Sendo-me presente o vosso officio, e o da Junta que segundo as minhas reaes ordens convocastes para dar principio ao grande estabelecimento de povoar os Campos de Guarapuava, de civilisar os indios barbaros, que infestam aquelle territorio, e de pôr em cultura todo o paiz que de uma parte vai confinar com o Paraná, e da outra forma as cabeceiras do Uruguay que, depois rega o paiz de Missões, e communica assim com a Capitania do Rio Grande; e tendo em consideração tudo o que lhe expuzestes e os votos dos Deputados da mesma Junta: hei por bem conformar-me com os acertados e bem fundados votos dos Coroneis João da Costa Ferreira, e Joseph de Arroche Toledo Randon, que vos ordeno e a Junta sirvam de base ao plano que deveis seguir e organisar para realizardes as minhas paternaes vistas, e portanto considerando que não é conforme aos meus principios religiosos, e politicos o querer estabelecer a minha autoridade nos Campos de Guarapuava, e territorio adjacente por meio de mortandades e crueldades contra os Indios, extirpando as suas raças, que antes desejo adiantar, por meio da religião e civilisação. até para não ficarem desertos tão dilatados e immensos sertões, e que só desejo usar da força com aquelles que offendem os meus Vassallos, e que resistem aos brandos meios de civilisação que lhes mando offerecer: sou servido ordenar-vos que prescreveis no meu real nome, ao Commandante que segundo vossa proposta tive por bem nomear para dirigir esta expedição que nos primeiros encontros que tiver com os bugres, ou outros quaesquer indios faça toda a diligencia para aprisionar alguns, os quaes tratará bem, e vestirá de camisas e outro vestuario, e fazendo-lhes persuadir pelos linguas que se lhes não quer fazer mal, e antes se deseja viver em paz com elles e defendel-os de seus inimigos, que então os largue e deixe ir livres para que vão dizer isso mesmo aos indios da sua especie com quem vivem, que dando-se o caso de encontrar os seus arranchamentos não lhes deite fogo nem faça violencia ás mulheres e crianças que nos mesmos se acharem antes lhes dêm camisas, e façam persuadir pelos linguas que nenhum mal se hade fazer ao indio pacifico habitador do mesmo territorio: que ao mesmo Commandante seja muito recommendado o vigiar que a sua tropa não tenha communicação com as indias, nem saiam de noite fóra de recinto, castigando severamente todos os que desobedecerem a estas minhas reaes ordens, e vierem assim a serem a causa de desordens, e desgraças; tendo o Commandante sempre presente que deve tratar os indios como filhos a respeito do castigo que merecem, porém não se fiando nunca, nem descuidando, visto que a experiencia tem mostrado que os povos barbaros, ou por um mal entendido, ou por qualquer accidente cahem em actos de violencia não esperados, e levam então sem motivo a sua crueldade e vingança a um ponto superior a toda a expectação. Será vosso cuidado recommendar ao Commandante da expedição que tome todos estes meios antes de vir aos da força que só praticará depois que experimentar a inutilidade destes, tendo tambem todo o cuidado em que as casas das povoações que for erigindo de novo sejam espaçadas umas das outras para que se os indios lançarem fogo a algumas dellas, as outras se possam salvar, cobertas quanto possivel for de telhas, e sempre rodeadas de algum fosso ou trincheira de madeira que assuste o indio roubador. Ao mesmo Commandante ordenareis que quando seja obrigado a declarar a guerra aos indios, que então proceda a fazer e deixar fazer prisioneiros de guerra pelas bandeiras que elle primeiro autorisar a entrar nos campos, pois sem essa permissão nehum bandeira, poderá entrar, nem fazer prisioneiros os indios que encontrar, bem entendido que esta prisão ou captiveiro só durará 15 annos contados desde o dia em que forem baptisados e desse acto religioso que se praticará na primeira freguezia por onde passarem se lhes dará certidão na qual se declare isso mesmo exceptuando porém os prisioneiros homens e mulheres de menor idade pois que nesses o captiveiro dos 15 annos se contará ou principiará a correr aos homens da idade de 14 annos, e nas mulheres da idade de 12 annos, declarando tambem que o proprietario do indio guardará sempre a certidão para mostrar o tempo de captiveiro que elle deve soffrer, e ficará exposto a declarar-se livre o indio, si acaso perder a certidão e não puder tirar outra, bem entendido que os serviços do indio prisioneiro de guerra poderão vender-se de uns a outros proprietarios pelo espaço de tempo que haja de durar o seu captiveiro, e segundo mostrar a certidão que sempre o deve acompanhar. Os prisioneiros de guerra feitos pela tropa á excepção daquelles que for necessario deixar para o meu real serviço, no que recommendareis ao Commandante se haja com a maior moderação, pois que desejo que esta não sirva para desanimar a Tropa de Linha e Miliciana do bom serviço que espero me faça nesta importante expedição. Muito vos hei por recommendado que fazendo partir o Commandante com a Tropa de Linha e Artilharia de calibre tres, que julgardes, e comvosco a Junta, proporcional á expedição intentada além da Tropa Mliciana, façais juntamente partir dous religiosos ou sacerdotes de zelo exemplar, e de luzes que sejam encarregados não só de catechisar, baptisar e instruir os indios, mas de vigiar que com elles se não pratique ciolencia alguma, senão aquella que for necessaria, para repellir a sua natural rudeza e barbaridade. Autorisareis ao Commandante para que além das sesmarias concedidas ao Governo para repartir os terrenos devolutos em proporções pequenas pelos povoadores pobres, pois que estes não teem forças para obterem sesmarias, e que reserve sempre uma legua de campo e mattos ao redor das povoações que for estabelecendo para commum logradio. Sendo muito util a communicação das Capitanias de S. Paulo e Rio Grande pelos campos que vertem para o Uruguay, e passam perto do Paiz de Missões; ordeno-vos que vos entendais com o Governador do Rio Grande, como tambem lhe mando directamente significar, para que ambas as Capitanias nos seus respectivos territorios e dentro dos limites do rio das Pelotas, ou pelo alto da Serra como dantes era, concorram com os meios necessarios a fazer esta estrada quanto antes transitavel, de maneira que se consiga assim uma mais facil communicação das duas Capitanias, e por esse meio com esta Capitania que assim communicará com ambas mais facilmente. Não sendo possivel distrahir cousa alguma das rendas da Capitania de S. Paulo, que todas se acham applicadas a objectos de maior urgencia, sou servido ordenar, que pelo espaço de 10 annos se cobre no Registro de Sorocaba um novo tributo de 200 réis nos primeiros cinco annos sobre toda a cabeça de gado vaccum e cavallar que passar pelo mesmo Registro, vindo do districto de Itapetinga inclusive para o Sul, e findos os primeiros cinco annos, a qual será applicada para e simplesmente á nova expedição que tenho ordenado, e para esse fim ordenareis á Junta da Fazenda, para entregar o producto da mesma á nova Junta de Guarapuava de que vos creei Presidente, para que ella proceda a fazer a devida applicação para as sobreditas despezas. Conformando-me com a vossa proposta fui servido nomear a Diogo Pinto de Portugal para Commandante desta expedição, e por este motivo o nomeio Tenente Coronel do Regimento de Milicias do que era Sargento-Mór de Cavallaria, esperando que se destinga pelo zelo com que hade promover a grande comissão de que o encarregareis e ao mesmo ordenareis que faça concorrer os fazendeiros da Coritiba e Campos Geraes proporcionalmente ás suas forças com alguns escravos para a abertura da estrada, que obrigue tambem os Fazendeiros concorrer segundo suas posses com gados para os trabalhadores, e os lavradores com farinha e feijões, mas tudo isto com tal moderação que não dê logar a queixa alguma. Igualmente fareis declarar que toda a pessoa que quiser ir povoar os Campos de Guarapuava não será constrangida pelo espaço de seis annos a pagar divida alguma que deva á Fazenda Real, e que pelo tempo de 10 annos não pagará dizimo das terras novas que rotear, nem outro direito parochial, se não o que for necessario para o mantenimento e trato e trato das Curas, que alli se estabelecerem. Igualmente vos ordeno que façais remetter para os Campos de Guarapuava todos os criminosos e criminosas que forem sentenciados a degredo, cumprindo alli todo o tempo do seu degredo. Assim o cumprireis e fareis executar não obstante quaesquer leis e regimentos em contrario que todos hei aqui por derogados, como se delles fizesse expressa menção. Escripta no Palacio do Rio de Janeiro em o 1º de Abril de 1809. PRINCIPE Para Antonio Joseph da Franca e Horta.</foreign>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_1_v__2520.tif" n="01_v" xml:id="ZM_S_I_Sellow_Exk_16_01_v"/>
<div><head>Seite 01, verso (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_01_v"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v siehe Transkript S. 16_1r</note>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_2_r__2521.tif" n="02_r" xml:id="ZM_S_I_Sellow_Exk_16_02_r"/>
<div><head>Seite 02, recto (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_02_r"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v siehe Transkript S. 16_1r</note>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_2_v__2522.tif" n="02_v" xml:id="ZM_S_I_Sellow_Exk_16_02_v"/>
<div><head>Seite 02, verso (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_02_v"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v siehe Transkript S. 16_1r</note>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_3_r__2523.tif" n="03_r" xml:id="ZM_S_I_Sellow_Exk_16_03_r"/>
<div><head>Seite 03, recto (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_03_r"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v siehe Transkript S. 16_1r</note>
</ab></div>
<pb facs="http://gbif.naturkundemuseum-berlin.de/CDV2018/Sellow/ZM_S_I_Sellow_Exk_16_3_v__2524.tif" n="03_v" xml:id="ZM_S_I_Sellow_Exk_16_03_v"/>
<div><head>Seite 03, verso (Bericht 16)</head><ab corresp="#ZM_S_I_Sellow_Exk_16_03_v"> <note resp="#CE_UM" type="editorial">Bericht 16 in portugiesischer Sprache; vollständige Transkription für Seiten 16_1r bis 16_3v siehe Transkript S. 16_1r</note>
</ab></div>
</body>
</text>
</TEI>