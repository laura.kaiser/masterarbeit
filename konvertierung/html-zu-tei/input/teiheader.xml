<teiHeader>
    <fileDesc>
        <titleStmt resp="#LK">
            <title xml:lang="de" xml:id="de_title" level="u">Exkursionsberichte von Friedrich Sellow (digitale Transkripte) </title>
            <title xml:lang="en" corresp="#de_title">Field trip reports written by Friedrich Sellow (digital transcriptions)</title>
            <author xml:id="FS">Friedrich Sellow</author>
            <respStmt xml:id="CE_UM">
                <name>Carsten Eckert</name>
                <name>Ulrich Moritz</name> 
                <resp>Transkription und Edition</resp>            
            </respStmt>
            <respStmt xml:id="LK">
                <name>Laura E. Kaiser</name>
                <resp>Konvertierung in TEI-konformes Markup</resp>
            </respStmt>
        </titleStmt>
        <publicationStmt resp="#LK">
            <authority>Museum für Naturkunde, Leibniz-Institut für Evolutions- und Biodiversitätsforschung</authority>
            <date>2020</date>
            <availability status="restricted">
                <licence target="https://creativecommons.org/licenses/by-sa/3.0/de/"> 
                    Dieses Werk ist lizenziert unter einer Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Deutschland Lizenz.
                </licence>
            </availability>
        </publicationStmt>
        <sourceDesc>
            <bibl>Exkursionsberichte der Reisen durch Brasilien und Uruguay von Friedrich Sellow, 1822-1831</bibl>
        </sourceDesc>
    </fileDesc>
    <encodingDesc resp="#LK">
        <projectDesc>
            <p>Die Texte wurden von Carsten Eckert und Ulrich Moritz für die wissenschaftliche Nutzung durch die Historische Arbeitsstelle des Museum für Naturkunde, Leibniz Institut für Evolutions- und Biodiversitätsforschung transkribiert.</p>
            <p>Im Rahmen einer Masterarbeit wurden die Transkripte aus dem .doc-Format (MS Word) in ein TEI-konformes XML-Dokument überführt, dem das im verknüpften XML Schema spezifizierte Datenmodell zugrunde liegt.</p>
        </projectDesc>
        <editorialDecl>
            <p>Sämtliche editorische Eingriffe wurden in den ursprünglichen Transkripten kenntlich gemacht. Alle dieser Markierungen wurden mit - soweit wie möglich - differenzierenden Tags kenntlich gemacht (siehe die beschriebenen Besonderheiten in &lt;tagsDecl&gt;.</p>
            <p>Silbentrennung, Zeilenumbrüche und Interpunktion wurden aus dem Original übernommen.</p>
        </editorialDecl>
        <tagsDecl partial="true">
            <rendition scheme="css" xml:id="et">margin-left:4em;</rendition>
            <rendition scheme="css" xml:id="red">color:red</rendition>
            <rendition scheme="css" xml:id="r">text-align:right;</rendition>
            <rendition scheme="css" xml:id="c">text-align:center;</rendition>
            <rendition scheme="css" xml:id="s">text-decoration:line-through</rendition>
            <rendition scheme="css" xml:id="sup">vertical-align:super; font-size:.7em</rendition>
            <rendition scheme="css" xml:id="u">text-decoration:underline</rendition>
            <rendition scheme="css" xml:id="v">float:left; transform:rotate(270deg);</rendition>
            <rendition scheme="css" selector="body">text-align:justify;</rendition>
            <namespace name="https://tei-c.org/ns/1.0/">
                <tagUsage gi="reg" resp="#LK">
                    &lt;reg&gt;-Elemente enthalten vom Original abweichende Schreibweisen. Diese umfassen einerseits, wie entsprechend der TEI Richtlinien für dieses Element vorgesehen, die Normalisierung untypischer Schreibweisen, darüber hinaus jedoch mitunter auch die Korrektur falscher Schreibweisen.
                    Diese Ungenauigkeit ist der ursprünglichen Transkription geschuldet, die keine Kodierung entsprechend der TEI-Richtlinien vorsah und entsprechend keine Differenzierung der editorischen Eingriffe vorgenommen hat. Da die Konvertierung nicht von den Transkribierern selbst, sondern von anderer Hand und weitgehend automatisiert vorgenommen wurde, wurden derartige Korrekturen vereinfachend ebenfalls mittels &lt;reg&gt; annotiert, was gegenüber einer (aufgrund mangelnder Expertise bezüglich historischer Schreibweisen) möglicherweise irrtümlichen Auszeichnung als korrigierte Form (&lt;corr&gt;) als geeignetere Strategie erscheint, die Konformität der Kodierung gegenüber dem TEI Abstract Model zu gewährleisten.
                    Eventuelle spätere Versionen dieser kodierten Texte sollten diesen Mangel beheben, indem Korrekturen innerhalb von &lt;corr&gt;-Elementen stehen und die originale Form entsprechend mit &lt;sic&gt; (anstatt derzeit &lt;orig&gt;) annotiert wird.
                </tagUsage>
                <tagUsage gi="supplied" resp="#LK">
                    &lt;supplied&gt;-Elemente kennzeichnen editorische Ergänzungen. Da in den ursprünglichen Transkripten absichtlich abgekürzte Schreibweisen nicht eindeutig von (aufgrund vergessener Zeichen/Wörter) falschen Schreibweisen unterscheidbar waren, wurden jegliche editorischen Einfügungen durch ein &lt;supplied&gt;-Element gekennzeichnet, unabhängig davon, ob sie dem Zweck der Auflösung von Abkürzungen oder der Korrektur von Fehlern durch ausgelassenen Text bzw. Zeichen folgen.
                    In einer künftigen Version sollte diese Ungenauigkeit behoben werden, indem lediglich offensichtlich beabsichtigte Auslassungen mittels des &lt;supplied&gt;-Elements ausgezeichnet werden. Falsche Schreibweisen sollten korrigiert werden, indem die korrekte Schreibweise innerhalb eines &lt;corr&gt;-Elements neben der Original-Variante innerhalb eines &lt;sic&gt;-Elements steht, die wiederum beide von einem &lt;choice&gt;-Element umgeben sind. Abkürzungsauflösungen sollten dann anhand des &lt;expan&gt;-Tags ausgezeichnet werden, der neben dem abgekürzten originalen Textteil als &lt;abbr&gt;-Element innerhalb eines &lt;choice&gt;-Elements steht.
                </tagUsage>
                <tagUsage gi="seg" resp="#LK">
                    &lt;seg&gt;-Elemente wurden für die Codierung thematisch kategorisierter Textpassagen verwendet. Darin enthaltene Namen sind aufgrund der automatischen Textcodierung noch nicht als solche codiert worden, weil einerseits nicht zwischen Textpassagen und Namen dieser thematischen Abschnitte vorgenommen wurde und andererseits auch keine automatisierte Zuordnung zum übergeordneten type-Attribut der &lt;name&gt;-Elemente möglich gewesen wäre. Bezüglich der &lt;seg&gt;-Elemente wäre es daher wünschenswert, in einer eventuellen künftigen Version Namen darin separat mit einer entsprechenden type- und subtype-Kategorisierung auszuzeichnen.
                    Darüber hinaus wurden &lt;seg&gt;-Elemente behelfsweise für die Auszeichnung von Messdaten verwendet, weil die fachliche Expertise zur feineren Codierung mittels &lt;measure&gt;- bzw. &lt;measureGrp&gt;-Elementen mit den zugehörigen Attributen fehlte. In künftigen Überarbeitungen sollte auch diese Unschärfe möglichst behoben werden.
                </tagUsage>
                <tagUsage gi="foreign">
                    Das &lt;foreign&gt;-Element wird in der derzeitigen Codierung ohne die Angabe der Sprache im xml:lang-Attribut verwendet, weil die Unterscheidung zwischen portugiesisch und spanisch in den Transkripten nicht vorhanden war. In künftigen Überarbeitungen sollte diese Information für &lt;foreign&gt;-Elemente ergänzt werden.
                </tagUsage>
            </namespace>
        </tagsDecl>
        <refsDecl>
            <p>Jeder Exkursionsbericht ist über das @n-Attribut eines &lt;text&gt;- wie auch body-Elements eindeutig identifizier- und adressierbar.</p>
            <p>Ebenso sind Seitenbeginne innerhalb des @n-Attributs jedes &lt;pb&gt;-Elements nummeriert und identifizierbar.</p>
            <p>Darüber hinaus ist jede Seite innerhalb eines &lt;ab&gt;-Element über dessen @xml:id-Attribut eindeutig identifizierbar. Die jeweilige ID folgt dem Muster des regulären Ausdrucks'ZM_S_I_Sellow_Exk_[0-9]{2}_[0-9]{2}a?(_(r|v))?' und stellt die hausinterne Signatur innerhalb des Museums für Naturkunde dar.</p>
        </refsDecl>
    </encodingDesc>
    <profileDesc>
        <handNotes>
            <handNote corresp="#FS">
                <p>Handschrift Friedrich Sellows</p>
            </handNote> 
            <handNote xml:id="CAT">
                <p>Handschrift Carl August Tennes</p>
            </handNote> 
            <handNote xml:id="unknown">
                <p>Handschrift einer unbekannten Person (bzw. Identifizierung der Handschrift nicht angegeben)</p>
            </handNote>
        </handNotes>
    </profileDesc>
    <revisionDesc>
        <change when="2020-06-29" who="#LK">
            <p>Diese Änderung beinhaltet die Konvertierung der Transkripte aus dem doc-Format in TEI P5 konforme XML-Daten, wobei auch die editorischen Eingriffe der Transkribierer soweit möglich spezifiziert wurden.</p>
        </change>
    </revisionDesc>
</teiHeader>