import os
import re
import bs4
import pandas
import itertools
from shutil import copyfile

nontei_in_name = 'manuskripte.html'
header_name = 'teiheader.xml'
markers_name = 'markierungen.html'
facsimile_table_name = 'faksimile_zuordnung.xlsx'
config_xlsx_name = 'konfiguration.xlsx'

file_dir = os.path.abspath(os.path.dirname(__file__))

nontei_in_file = open(os.sep.join([file_dir,'input',nontei_in_name]),encoding='utf-8',mode='r')
data = nontei_in_file.read()
nontei_in_file.close()

header_file = open(os.sep.join([file_dir,'input',header_name]),encoding='utf-8',mode='r')
new_header = header_file.read()
header_file.close()

markers_file = open(os.sep.join([file_dir,'input',markers_name]),encoding='utf-8',mode='r')
markers_doc = markers_file.read()
markers_file.close()

facsimile_table = os.sep.join([file_dir,'input',facsimile_table_name])

config_xlsx = os.sep.join([file_dir,config_xlsx_name])

out_dir = os.sep.join([file_dir,'output'])
os.makedirs(out_dir,exist_ok=True)
tei_out = open(os.sep.join([out_dir,'manuskripte_tei.xml']),encoding='utf-8',mode='w')




# angegebene Optionen einlesen

options_sheet = pandas.read_excel(config_xlsx,sheet_name='Optionen')
options = options_sheet.to_dict(orient='list')

# Absatzmarkierung ja/nein
if options['absaetze_beibehalten'][0] == 'ja':
    keep_p_tags = True
else:
    keep_p_tags = False

# Markierung von Zeilenumbrüchen
lb_marker = options['markierungszeichen_zeilenumbruch'][0]
if lb_marker == 'none' or lb_marker == 'None':
    lb_marker = None
    lb_marker_pos = None
else:
    if lb_marker in ['\\','^','$','.','|','?','*','+','(',')','[',']','{','}']:
        lb_marker = '\\' + lb_marker
    if options['zeilenumbruch_position'][0] == 'zeilenende':
        lb_marker_pos = 'end'
    else:
        lb_marker_pos = 'beginning'

# Vereinheitlichung von Signaturen (Sellow-Berichte)
if options['normalisierung_signaturen'][0] == 'ja':
    norm_sign = True
else:
    norm_sign = False
if options['faksimile_zuordnung'][0] == 'ja':
    facs_attribution = True
else:
    facs_attribution = False

# Schemata
schemas = [i for i in options['schemata'] if str(i) != 'nan']



# Funktion für Aufbereitung der OpenOffice-Outputs: irrelevante Tags entfernen bzw. ersetzen; XML-Deklaration & Wurzeltag anpassen
def oo_format(text,doc):

    # alle Tagnamen in Versalien ändern in Kleinbuchstaben
    tags = re.compile('(</?)(.+?)(/?>)')
    text = tags.sub(lambda match: match.group(1) + match.group(2).lower() + match.group(3),text)

    # XML-Deklaration statt DOCTYPE
    xmldecl = '<?xml version="1.0" encoding="utf-8"?>'
    doctype = re.compile('<!doctype.+?>')
    text = doctype.sub(xmldecl,text)

    # head (der nicht wohlgeformt ist) mit zugehörigen Metatags etc. durch (wohlgeformten) Platzhalter-Header ersetzen
    head = re.compile('((<head>)|(<teiHeader>)).*<body.*?>',re.S)
    text = head.sub('<teiHeader>Platzhalter</teiHeader><body>',text)
    after_body = re.compile('(?<=</body>).*?(?=(</html>)|(</TEI>))',re.S)
    text = after_body.sub('',text)

    # html-Wurzeltag wird zu TEI-Wurzeltag
    text = text.replace('<html>','<TEI>')
    text = text.replace('</html>','</TEI>')

    # br- und ggf. p-Tags löschen
    text = re.sub('(<br>)|<lb/>','',text)
    if keep_p_tags == False or doc == 'markers_doc':
        p_tags = re.compile('(</?p.*?>)')
        text = p_tags.sub('',text)
    if keep_p_tags == True:
        p_tags = re.compile('(<p.*?>)')
        text = p_tags.sub('<p>',text)

    # Kursivschrift z.T. durch <em>, z.T. durch <i> ausgezeichnet - vereinheitlichen (ausschließlich <i>)
    italic = re.compile('(</?)em(.*?>)')
    text = italic.sub(lambda match: match.group(1) + 'i' + match.group(2),text,re.S)
    
    # <COL>-Tags von Tabellen entfernen
    col = re.compile('</?col.+?>')
    text = col.sub('',text)

    # Problem in OpenOffice-html: Attributwerte z.T. nicht mit Anführungszeichen markiert; wird hier korrigiert, da sonst bs4 zugehörigen Tag nicht als solchen komplett identifiziert, sondern Attribute in String umwandelt
    tags_open = re.compile('<[^/].*?/?>')
    bug_attr = re.compile('([^ ]+?=)([^" >]+)')
    text = tags_open.sub(lambda match: bug_attr.sub(lambda attr: attr.group(1) + '"' + attr.group(2) + '"',match.group(0)),text)

    return text

# Dictionary für relevante Markierungen

print('Datei mit Markierungen wird eingelesen...')
markers = oo_format(markers_doc,'markers_doc')
markers_tree = bs4.BeautifulSoup(markers,'xml')
relevant_markers_list = [i.parent for i in markers_tree.body.find_all(string=True) if i.parent.name !='body']
markers_dict = {}
for marker in relevant_markers_list:
    marker_key = marker.name +';'+','.join([(attr_name + '=' + marker.attrs[attr_name].replace(' ','')) for attr_name in marker.attrs])
    marker_string_ws = re.sub('\s',' ',marker.string)
    tags = marker_string_ws.split('; ')
    tags_dict = {}
    for tag in tags:
        list_keyval = tag.split(' ')
        name = list_keyval[0]
        del list_keyval[0]
        if len(list_keyval) > 0:
            attr_dict = {val.split('=')[0]: val.split('=')[1] for val in list_keyval}
        else:
            attr_dict = {}
        tags_dict[name] = attr_dict
    markers_dict[marker_key] = tags_dict
print('Fertig.')





# Aufbereitung der Textdaten


print('Zu konvertierende Textdaten werden eingelesen...')
data = oo_format(data,'data_doc')
print('Fertig.')


# Bestehende Tags aufbereiten

data_tree=bs4.BeautifulSoup(data,'xml')

print('Vorhandene Tags werden aufbereitet...')


# Tabellen-Elemente in Elemente mit TEI-Tags überführen

for table in data_tree.find_all('table'):
    for element in table.find_all(re.compile('(tr)|(td)')):
        span_attrs = {}
        if element.has_attr('colspan'):
            span_attrs['cols'] = element['colspan']
        if element.has_attr('rowspan'):
            span_attrs['rows'] = element['rowspan']
        if element.name == 'tr':
            element.name = 'row'
        elif element.name == 'td':
            element.name = 'cell'
        element.attrs = span_attrs
    rows = len(table.find_all('row'))
    cols = 0
    for cell in table.row.find_all('cell'):
        if cell.has_attr('cols'):
            cols = cols + int(cell['cols'])
        else:
            cols = cols + 1
    table.attrs = {'rows':str(rows),'cols':str(cols)}


# Formatierungen aus Word-Transkripten in (vorläufige) Tags übertragen

def tag_is_relevant_marker(element):
    name_and_attrs = element.name +';'+','.join([(attr_name + '=' + element.attrs[attr_name].replace(' ','')) for attr_name in element.attrs])
    if name_and_attrs in markers_dict:
        return True
    else:
        return False


# relevante Markierungen in neue vorläugige Tags umsetzen - übergangsweise gleicher Tagname, Markierung als Attributwert (als String aus bisherigem Tagnamen und Attributen mit Werten ohne Whitespace - gleiches Muster wie in oben erstelltem Dictionary der Markierungen aus Markierungsdatei für spätere Übersetzung in finale TEI-Tags), um folgende Vereinfachungen vornehmen zu können
for element in data_tree.body.find_all(tag_is_relevant_marker):
    name_and_attrs = element.name +';'+','.join([(attr_name + '=' + element.attrs[attr_name].replace(' ','')) for attr_name in element.attrs])
    element.name = 'annot'
    element.attrs = {}
    element['ident'] = name_and_attrs
print('Fertig.')


# alle nicht-relevanten verbleibenden Tags innerhalb des body-Elements (d.h. wenn nicht <hi> mit @ident oder ggf. <p>) löschen 

print('Überflüssige Tags werden gelöscht...')

# Text aufteilen, sodass Löschen nur innerhalb des Bodys erfolgt
data = str(data_tree)
data_splitted = data.split('<body>')
data_splitted_2 = data_splitted[1].split('</body>')

# Funktion, um relevante und daher beizubehaltende Tags zu identifizieren und Irrelevante zu löschen
def remove_tags(tag):
    if tag.group(2) == 'p' and keep_p_tags == True:
        return tag.group(0)
    elif tag.group(2) in ['table','row','cell']:
        return tag.group(0)
    elif tag.group(2) == 'annot':
        return tag.group(0)
    else:
        return ''

# all_tags = re.compile('(</?)(([^ \s>])+?)( [^>]+?)?>',re.S)
all_tags = re.compile('(</?)(([^ \s>])+)( [^>]+)?>',re.S)
data_body_new = all_tags.sub(lambda match: remove_tags(match),data_splitted_2[0])
data = data_splitted[0] + '<body>' + data_body_new + '</body>' + data_splitted_2[1]
data_tree = bs4.BeautifulSoup(data,'xml')
print('Fertig.')


# Verschachtelung der Tags in OpenOffice-Writer-HTML nicht stimmig: zusammenhängend markierte Bereiche haben bei mehrfachen, einander überlagernden Markierungen den gleichen Tag mehrfach hintereinander statt durchgängig (z.T. auf unterschiedlichen hierarchischen Ebenen) - wird hier korrigiert

print('Doppelte Tags aus Open Office Writer werden gelöscht...')

# wenn Markierungselement einziges Element von Eltern-Markierungselement ist, werden Markierungswerte miteinander abgeglichen und zusammengeführt, sodass Ausgangsmarkierungstag entfernt werden kann
def remove_double_tags(element,attr):
    if element.parent.name == element.name:
        element_desc = [i for i in element.descendants]
        parent_desc = [i for i in element.parent.descendants]
        if element_desc == parent_desc[1:]:
            element_attr = set(element[attr].split(sep=' '))
            parent_attr = set(element.parent[attr].split(sep=' '))
            parent_attr.update(element_attr)
            element.parent[attr] = ' '.join(list(parent_attr))
            remove_double_tags(element.parent,'ident')
            element.unwrap()           

for annot in data_tree.find_all('annot'):
    remove_double_tags(annot,'ident')    




# Funktion, um gleichnamige Geschwisterelemente unter Berücksichtigung der Attributwerte zusammenzuführen - d.h. Zusammenführen von NEBENEINANDERstehenden Markierungen (bei gleichen Attributwerten)
def sibl_proof(element,attr):
    element_attr_set = set(element[attr].split(sep=' '))
    for sibling in element.next_siblings:
        if isinstance(sibling,bs4.element.NavigableString):
            if re.fullmatch('(\s| )+',sibling):
                continue
            else:
                break
        elif sibling.is_empty_element is True:
            continue
        # bei gemeinsamen Attributwerten mit Geschwister-Element gleichen Namens wird in letzterem Liste von Attribut-Werten um gemeinsamen Wert reduziert; neuer Element-Tag mit Attribut-Werten des Ausgangs-Elements ohne den/die gemeinsamen AttributWert(e) wird um Elementinhalte des Ausgangselements gesetzt; Ausgangselement bekomment die Attributwerte, die es mit Geschwisterelement gemeinsam hatte, und wird um Geschwisterelement (inkl. ggf. dazwischenliegendem Whitespace und Leerelementen) erweitert; falls Geschwisterelement keine eigenen Attributwerte mehr besitzt ist es überflüssig und wird entfernt
        elif sibling.name == element.name:
            sibling_attr_set = set(sibling[attr].split(sep=' '))
            if sibling_attr_set.isdisjoint(element_attr_set) == False:  
                element_attr_diff = element_attr_set - sibling_attr_set
                sibling_attr_diff = sibling_attr_set - element_attr_set
                sibling_attr_new = ' '.join(sibling_attr_diff)
                sibling[attr] = sibling_attr_new
                if len(element_attr_diff) > 0:
                    diff_element_tag = data_tree.new_tag(element.name,attrs={attr : ' '.join(element_attr_diff)})
                    element_attr_comm = element_attr_set - element_attr_diff
                    element[attr] = ' '.join(element_attr_comm)
                    element.insert(0,diff_element_tag)
                    diff_element_tag.extend(diff_element_tag.next_siblings)
                wrap = [i for i in itertools.takewhile(lambda x: x is not sibling, element.next_siblings)]
                for wrap_element in wrap:
                    element.append(wrap_element)
                element.append(sibling)
                if len(sibling[attr]) == 0:
                    sibling.unwrap()
                # Rekursion - für weitere Geschwister des (nun erweiterten) hi-elements
                sibl_proof(element,attr)
            else:
                break
        elif sibling.name != element.name:
            break
        else:
            break

for annot in data_tree.find_all('annot'):
    sibl_proof(annot,'ident')

print('Fertig.')




# 'Übersetzung' der Markierungen (aktuell in Attributwerten in @ident in <annot>) in TEI-Tags

print('Bestehende Tags werden in TEI-Tags umbenannt...')

# Funktion für Sortierung der Tagnamen (Reihenfolge der zu erstellenden Tags - <hi> zuletzt (s.u.))
def hi_last(name):
    if name != 'hi':
        return 0
    else:
        return 1

# Funktion für neue Tags: für jeden Tag, der als Text in Markierungsdatei spezifiziert wurde, wird neuer Tag mit den entsprechenden Attributen erzeugt und um den Elementinhalt des Übergangselements gesetzt; Übergangstag wird im Anschluss entfernt
def set_tags_from_format(element,el_attr):
    for element in data_tree.find_all(element):
            idents = element[el_attr].split(sep=' ')
            new_tags = []
            for ident in idents:
                for tag in markers_dict[ident]:
                    new_tag = data_tree.new_tag(tag,attrs = markers_dict[ident][tag])
                    new_tags.append(new_tag)
            # hi-Tags sollen zum Schluss gesetzt werden (andersherum wäre lt. Datenmodell auch zulässig; eher methodische Entscheidung: inhaltliche Tags wurden nachträglich bei Transkription gesetzt, visuelle Texteigenschaften (Formatierungen) beziehen sich auf Original - semantische Tags umfassen daher eher Hervorhebungen als andersherum)
            new_tags.sort(key=hi_last)
            new_tags_added = []
            for new_tag in new_tags:
                if new_tag.name in new_tags_added:
                    parent = element.find_parent(new_tag.name)
                    for attr in new_tag.attrs:
                        if attr in parent.attrs:
                            parent[attr] = parent[attr] + ' ' + new_tag[attr]
                        else:
                            parent[attr] = new_tag[attr]
                    new_tags_added.append(new_tag.name)
                else:
                    element.wrap(new_tag)
            element.unwrap()

set_tags_from_format('annot','ident')

print('Fertig.')




# Signatur in @xml:id des pb-Tags verwenden, Seitennr. des Berichts als @n-Attribut

print('Signaturen werden aufbereitet...')

data = str(data_tree)

# für den Fall, dass pb-Tags wegen unabsichtlicher Formatierung von Whitespace an falscher Stelle gesetzt wurden und schon jetzt ohne Inhalt sind (d.h. keine Signatur umschließen): löschen
pb_del = re.compile('<pb>( |\s)*?</pb>',re.S)
data = pb_del.sub('',data)

# ggf. mit Vereinheitlichung der Signatur
if norm_sign == True:
    pb = re.compile('<pb>[ \n]*?((?:ZM)?[ \n]*?_?S_I_Sellow_Exk[ \n]*?_)[ \n]*?([0-9]{1,2})[ \n]*?_[ \n]*?([0-9]{1,2})(a?)_?[ \n]*?(r|v)?[ \n]*?</pb>')
    def pb_id(match):
        # wenn Berichtsnummer in Signatur einstellig: auf zweistellig ändern
        if len(match.group(2)) == 1:
            sub1 = '0' + match.group(2)
        else:
            sub1 = match.group(2)
        # wenn Seitenzahl in Signatur einstellig: auf zweistellig ändern
        if len(match.group(3)) == 1:
            sub2 = '0' + match.group(3)
        else:
            sub2 = match.group(3)
        # wenn Zusatzbuchstabe nach Seitenzahl diesen mit Unterstrich abgrenzen
        if match.group(5) == None:
            sub3 = ''
        else:
            sub3 = '_' + match.group(5)
        # Zusammensetzung der neuen Signatur: ZM_S_I_Sellow_Exk_+{zweistellige Berichtsnummer}_{zweistellige Seitenzahl}_{r|v} und pb-Tag
        return '<pb xml:id="ZM_S_I_Sellow_Exk_'+ sub1 + "_" + sub2 + match.group(4) + sub3 + '" n="' + sub1 + '_' + sub2 + match.group(4) + sub3 + '"/>'

# alternativ ohne weitere Bearbeitung übernehmen
else:
    pb = re.compile('<pb>(.+?)</pb>',re.S)
    def pb_id(match):
        return '<pb xml:id="' + match.group(1) + '"/>'

data = pb.sub(pb_id,data)




# ggf. Zuordnung der Faksimile-Dateien durch @xml:id-Werte in <pb/> mittels aufbereiteter Metadatendatei und ggf. (falls keine p-Elemente vorhanden) ausgehend von <pb/>-Tags jede Seite mit ab-Tag umschließen und darin mit @corresp auf ID des zugehörigen <pb> verweisen

data_tree = bs4.BeautifulSoup(data,'xml')

if facs_attribution == True:
    xls = pandas.ExcelFile(facsimile_table)
    meta = xls.parse(xls.sheet_names[0])
    sign_dict = meta.to_dict(orient='list')
for pb in data_tree.find_all('pb'):
    # ggf. Faksimile-Zuordnung über Tabelle vornehmen
    if facs_attribution == True:
        if pb['xml:id'] in sign_dict:
            pb['facs'] = sign_dict[pb['xml:id']][0]
        else:
            pb['facs'] = 'N/A: ' + pb['xml:id']
            print('Der Seite mit der ID ' + pb['xml:id'] + ' konnte kein Faksimile zugeordnet werden.')
    # falls keine p-Tags verwendet werden: ab-Tag um jeden Seiteninhalt (nötig für TEI-Konformität - "Container-Element" für phrase-level-Inhalte)
    if keep_p_tags == False:
        ab_new_tag = data_tree.new_tag('ab',attrs={'corresp':'#' + pb['xml:id']})
        wrap = [i for i in itertools.takewhile(lambda x: x.name not in ['pb','body'],pb.next_siblings)]
        pb.insert_after(ab_new_tag)
        for element in wrap:
            ab_new_tag.append(element)
    else:
        # falls p-Tags vorhanden, pb-Leerelement nicht als Absatz markieren
        if pb.parent.name == 'p':
            pb.parent.unwrap()

print('Fertig.')

      


print('Elemente in eckigen Klammern werden in TEI-Tags überführt...')

# falls wissenschaftlicher Fachbegriff von eckigen Klammern umschlossen: editorische Ergänzung - <term> durch <note> mit Verantwortlichkeit in @resp umschließen, eckige Klammern entfernen

data_tree.smooth()

for term in data_tree.find_all('term'):
    previous_string = term.find_previous(string=True)
    next_string = term.find_next(string=True).find_next(string=True)
    previous_string_stripped = previous_string.rstrip('\n')
    next_string_stripped = next_string.lstrip('\n')
    if re.fullmatch('(.*?)\[',previous_string_stripped,re.S):
        previous_string_new = bs4.NavigableString(re.sub('(.*?)(\[)$','\g<1>',previous_string_stripped,re.S))
        next_string_new = bs4.NavigableString(re.sub('^(\])(.*?)','\g<2>',next_string_stripped,re.S))
        previous_string.replace_with(previous_string_new)
        next_string.replace_with(next_string_new)
        note_tag = data_tree.new_tag('note',attrs={'resp' : '#CE_UM'})
        term.wrap(note_tag)
    else:
        continue




# Behelfsannotationen in eckigen Klammern in entsprechende Elemente und Attribute umsetzen

data = str(data_tree)

# Dictionary für neue Tagnamen & Attribute aus xlsx-Datei
dict_new_tags = pandas.read_excel(config_xlsx,sheet_name='Tagnamen')
new_tag_names_dict_raw = dict_new_tags.to_dict(orient='list')
new_tag_names_dict = {}
lst_nt  = []
for key in new_tag_names_dict_raw:
    new_tag_names_dict[key.replace(' ','')] = new_tag_names_dict_raw[key]
    lst_nt.append(new_tag_names_dict_raw[key])
dict_new_attrs = pandas.read_excel(config_xlsx,sheet_name='Attribute')
new_attrs_dict_raw = {str(key): [str(i) for i in dict_new_attrs.to_dict(orient="list")[key] if str(i) != 'nan'] for key in dict_new_attrs.to_dict(orient="list")}
new_attrs_dict = {}
for key in new_attrs_dict_raw:
    new_attrs_dict[key.replace(' ','')] = new_attrs_dict_raw[key]

# öffnende eckige Klammer mit anschließend per Kommata getrenntem Text bis Doppelpunkt markieren 'Starttag' Behelfsauszeichnungen
open_pattern = re.compile('(\[([^\]]+?): ?)',re.S)
annotations_left = True
while annotations_left == True:
    match = open_pattern.search(data)
    annotations_raw = re.sub('\s',' ',match.group(2)).split(', ')
    annotations = [i.replace(' ','').strip() for i in annotations_raw]
    tag_name = ''.join(new_tag_names_dict[annotations[0]])
    # für Attribute und deren Werte pro Tag Dicitionary für spätere Angabe im Tag
    tags_attrs = {}
    attrs_string = ''
    # alle weiteren Begriffe ggf. ab zweiter Position
    if len(annotations) > 1:
        for attr in annotations[1:]:
            # Verweissysmbole, IDs und IDREFs haben anderes Format (weil sie keine einheitlichen Attributwerte haben) - Sonderbehandlung
            if re.fullmatch('Verweissymbol(.+)',attr):
                tags_attrs['n'] = [re.fullmatch('Verweissymbol(.+)',attr).group(1)]
            elif re.fullmatch('Bezug\((.+?)\)',attr):
                corresp_vals = re.fullmatch('Bezug\((.+?)\)',attr).group(1).split(';')
                if 'corresp' not in tags_attrs:
                    tags_attrs['corresp'] = []
                for val in corresp_vals:
                    val = '#' + val
                    tags_attrs['corresp'].append(val)
            elif re.fullmatch('ID\((.+?)\)',attr):
                tags_attrs['xml:id'] = [re.fullmatch('ID\((.+?)\)',attr).group(1)]
            # alle anderen Attributnamen und -werte werden über Dictionary aus xlsx-Datei entnommen
            else:
                for x in range(0,len(new_attrs_dict[attr]),2):
                    attr_name = new_attrs_dict[attr][x]
                    attr_val = new_attrs_dict[attr][x+1]
                    if attr_name in tags_attrs:
                        tags_attrs[attr_name].append(attr_val)
                    else:
                        tags_attrs[attr_name] = [attr_val]
        for key in tags_attrs:
            attrs_string = attrs_string + ' ' + key + '="' + ' '.join(tags_attrs[key]) + '"'
    dsplit_1 = data[:match.start()]
    dsplit_2 = data[match.end():]
    count = 1 
    # zu Behelfsannotation zugehörige schließende Klammer suchen
    for bracket in re.finditer('\[|\]',dsplit_2):
        if bracket.group(0) == '[':
            count = count + 1
        else:
            count = count - 1
            if count == 0:
                dsplit_2_1 = dsplit_2[:bracket.start()]
                dsplit_2_2 = dsplit_2[bracket.end():]
                dsplit_2_w_tag = dsplit_2_1 + '</' + tag_name + '>' + dsplit_2_2
                break
            else:
                continue
    # Neuzusammensetzung der Daten mit entsprechenden Tags
    data = dsplit_1 + '<' + tag_name + attrs_string + '>' + dsplit_2_w_tag
    if open_pattern.search(data) != None:
        annotations_left = True
    else:
        annotations_left = False




# String von gap enthält Ausmaß der nicht entzifferten Stellen - Umwandlung in Attribute mit entsprechenden Werten

gap_units_dict = {'Buchstabe':'chars','Buchstaben':'chars','Wort':'words', 'Worte':'words', 'Wörter':'words','Zeile':'lines','Zeilen':'lines','Seite':'pages','Seiten':'pages'}
gaps = re.compile('(<gap.*?)>(.+?)</gap>',re.S)
def gap_extent(match):
    extent = match.group(2).replace('\n',' ').strip().split(' ')
    extent_quant = extent[0]
    if extent[1] in gap_units_dict:
        extent_unit = gap_units_dict[extent[1]]
    else:
        extent_unit = 'N/A'
        print('Die folgende Angabe einer nicht entzifferten Stelle konnte keiner zulässigen Einheit zugeordnet werden:' + match.group(1))
    return match.group(1) + ' quantity="' + extent_quant + '" unit="' + extent_unit + '"/>'
data = gaps.sub(gap_extent,data)


 

# String von figure-Element enthält ggf. Beschreibung der Zeichnung - Umwandlung in entsprechendes figDesc-Element

figure = re.compile('(<figure.*?>)(.*?)(</figure>)',re.S)
def figure_desc(match):
    if re.fullmatch('(\s| )*',match.group(2),re.S):
        return match.group(1) + match.group(3)
    elif re.search('<head.*?>.+?</head>',match.group(2),re.S) != None:
        return match.group(0)
    else:
        return match.group(1) + '<figDesc>' + match.group(2) + '</figDesc>' + match.group(3)
data = figure.sub(figure_desc,data)




# für den Fall, dass <hi>-Elemente um andere hi-Elemente eingefügt wurden, Tags mit rendition-Werten ggf. zusammenführen

data_tree = bs4.BeautifulSoup(data,'xml')

for hi in data_tree.find_all('hi'):
    remove_double_tags(hi,'rendition')




# Annotation alternativer Textversionen (Original vs. editorische Eingriffe) mit umschließendem Tag zur Kennzeichnung als parallele Kodierungen, gemäß Angabe in Optionen-Sheet in xlsx 


def alternatives(given_tag,set_tag,parent,dat):
    dat.smooth()

    for element in dat.find_all(given_tag):

        # falls Alternativen schon korrekt annotiert: fertig
        if element.parent.name == parent:
            continue
        else:
            if re.fullmatch('[ \s]+',element.find_previous(string=True)):
                string_before = element.find_previous(string=True).find_previous(string=True)
                ws_between = True
            else:
                string_before = element.find_previous(string=True)
                ws_between = False
            element_before = string_before.parent
            wrapping_tag = dat.new_tag(parent)
            alternative = dat.new_tag(set_tag)
            # Sonderbehandlung für add/del bzw, del/add: beide Tags müssten gesetzt sein, damit sie umschlossen werden (wäre sonst uneindeutig, weil es auch reine Einfügungen und Löschungen ohne zugehörige Alternative (d.h. parallele Kodierung) gibt)
            if element.name == 'add' or element.name == 'del':
                if (element.name == 'add' and element_before.name == 'del') or (element.name == 'del' and element_before.name == 'add'):
                    element_before.wrap(wrapping_tag)
                    if ws_between == True:
                        wrapping_tag.append(element.find_previous(string=True))
                    wrapping_tag.append(element)
            # für alle anderen Elementpaare als parallele Kodierungen:
            else:
                # wenn zugehöriger Tag schon vorhanden: nur noch Elterntag um die beiden Alternativen setzen
                if element_before.name == set_tag:
                    element_before.wrap(wrapping_tag)
                    if ws_between == True:
                        wrapping_tag.append(element.find_previous(string=True))
                    wrapping_tag.append(element)
                elif element_before.find_parent(set_tag) != None:
                    element_before.find_parent(set_tag).wrap(wrapping_tag)
                    if ws_between == True:
                        wrapping_tag.append(element.find_previous(string=True))
                    wrapping_tag.append(element)
                # ansonsten wird davon ausgegangen, dass Alternativtag um einzelnes vorangehendes Wort gesetzt werden soll
                else:
                    string_splitted = string_before.split(sep=' ')
                    if len(string_splitted) == 1:
                        string_joined = ''
                        alternative.string = ''.join(string_splitted)
                    else:
                        string_joined = ' '.join(string_splitted[:-2]) + ' '
                        alternative.string = string_splitted[-1]
                    string_before.insert_after(alternative)
                    string_before.replace_with(string_joined)
                    alternative.wrap(wrapping_tag)
                    if ws_between == True:
                        wrapping_tag.append(element.find_previous(string=True))
                    wrapping_tag.append(element)
    return dat

for field in options['parallele_kodierung']:
    tags = field.split(sep=',')
    data_tree = alternatives(tags[1],tags[0],tags[2],data_tree)




# Annotation der linebeginnings

# Zeichen, die für Zeilenumbruchmarkierung genutzt wurden, durch linebeginning-Tags (<lb/>) ersetzen

data = str(data_tree)
y = re.compile(lb_marker)
data = y.sub('<lb/>',data)
 

# <lb/> kennzeichnet "line beginning" - falls statt Zeilenanfängen Zeilenenden markiert wurden: <lb/> je zu Beginn statt am Ende der Seite/des Absatzes

data_tree = bs4.BeautifulSoup(data,'xml')

def lb_end_to_beginning(container_element,check_for_end):
    lb_new_first = data_tree.new_tag('lb')
    data_tree.find(container_element).insert(0,lb_new_first)
    for element in data_tree.find_all(container_element)[1:-2]:
        if len(element.find_all('lb')) > 0:
            lb_last = element.find_all('lb')[-1]
        else:
            continue
        if check_for_end == True:
            last = element.contents[-1]
            if lb_last == last or (lb_last == element.contents[-2] and isinstance(last,bs4.element.NavigableString) and re.fullmatch('[ \s]+',last)):
                element.find_next(container_element).insert(0,lb_last)
            else:
                continue        
        else:
            element.find_next(container_element).insert(0,lb_last)

data_tree.smooth()

if lb_marker_pos == 'end':
    if keep_p_tags == False:
        # bei Einfügen zu Beginn eines ab-Elements: Position 1, d.h. nach <pb/>
        lb_end_to_beginning('ab',False)
    else:
        # bei p-Element an Position 0, d.h. als erstes Element einfügen (falls letztes <lb/> wirklich am Ende eines p-Elements): 
        lb_end_to_beginning('p',True)




# Einrückungen (durch '[Leerraum]' markiert) annotieren

leerraum = re.compile('\s*\[Leerraum\]\s*')

for string in data_tree.find_all(string=leerraum):
    matches = re.findall(leerraum,string)
    string_splitted = re.split(leerraum,string)
    string_part_first = bs4.NavigableString(string_splitted[0])
    string.insert_before(string_part_first)
    for string_part in string_splitted[1:]:
        # Unterscheidung der @rendition-Werte: et-t für tabellenartige Abstände (innerhalb von Messungen), et für sonstige Einrückungen
        if string.parent.name == 'seg' and string.parent['type'] == 'measurement-data':
            leerraum_tag = data_tree.new_tag('hi',attrs={'rendition' : '#et-t'})
        else:
            leerraum_tag = data_tree.new_tag('hi',attrs={'rendition' : '#et'})
        leerraum_tag.string = string_part
        # Bisher Leerraum statt Einrückung annotiert - neues hi-Element muss unmittelbar folgenden String, der als 'eingerückt' gilt, enthalten
        string.insert_before(leerraum_tag)
    string.replace_with('')




# Auslassungen (mit '[Auslassung]' oder'[Freilassung]' transkribiert) in Leerelemente umsetzen

data = str(data_tree)
space = re.compile('\[\s*((Freilassung)|(Auslassung))\s*\]')
data = space.sub(' <space/> ',data)




# editorische Ergänzungen annotieren (verbliebene Buchstaben & Zeichen in eckigen Klammern)

supplied = re.compile('\[(.+?)\]',re.S)
data = supplied.sub('<supplied>\g<1></supplied>',data)

print('Fertig.')




# zusätzliche Attributwerte setzen

print('Zusätzliche Attribute werden ergänzt...')

data_tree = bs4.BeautifulSoup(data,'xml')
attrs_add = pandas.read_excel(config_xlsx,sheet_name="zusätzliche Attribute")
dict_attrs_add = {str(key): [str(i) for i in attrs_add.to_dict(orient="list")[key] if str(i) != 'nan'] for key in attrs_add.to_dict(orient="list")}
for element in data_tree.find_all(dict_attrs_add):
    for x in range(0,len(dict_attrs_add[element.name]),2):
        if ',' in dict_attrs_add[element.name][x]:
            attr_needed = set(dict_attrs_add[element.name][x].split(','))
            attrs_given = {i for i in element.attrs}
            if bool(attrs_given & attr_needed):
                continue
            else:
                attr = dict_attrs_add[element.name][x+1].split('=')
                element[attr[0]] = attr[1]
        elif element.has_attr(dict_attrs_add[element.name][x]):
            continue
        else:
            element[dict_attrs_add[element.name][x]] = dict_attrs_add[element.name][x+1]




# für Marginalien (nur im Original, d.h. mit @hand) mit Verweissysmbolen (@n): @anchored auf 'true' setzen, für alle anderen auf 'false'

def note_original(tag):
    if tag.name == 'note' and tag.has_attr('hand'):
        return True
    else:
        return False

for note in data_tree.find_all(note_original):
    if note.has_attr('n'):
        note['anchored'] = 'true'
    else:
        note['anchored'] = 'false'


print('Fertig.')




# allgemeine Textstruktur an TEI-Modell anpassen: Berichte / einzelne Texte in je mit body- und text-Element umschließen, Texte zusammen gruppieren

print('Daten werden an TEI-Grundstruktur angepasst...')

# bestehende Einteilung löschen

for element in data_tree.find_all(re.compile('(body)|(text)')):
    element.unwrap()


# basierend auf Nummerierung in pb-Elementen body- und text-Tags um je einen Bericht erstellen

report_nr = '00'
nr_facs = re.compile('([0-9]{2})_([0-9]{2}.*)')
for pb in data_tree.find_all('pb'):
    if pb.parent.name != 'body':
        report_nr_curr = nr_facs.match(pb['n']).group(1)
        body = data_tree.new_tag('body',attrs={'n' : report_nr_curr})
        pb.wrap(body)
        wrap = [i for i in itertools.takewhile(lambda x: x.name != 'pb' or (x.name == 'pb' and nr_facs.match(x['n']).group(1) == body['n']),body.next_siblings)]
        for content in wrap:
            body.append(content)
        for pb_wrapped in body.find_all('pb'):
            pb_wrapped['n'] = nr_facs.match(pb_wrapped['n']).group(2)
        text = data_tree.new_tag('text',attrs={'n':body['n']})
        body.wrap(text)


# alle Texte zusammen mit <group> umschließen, group auch nochmal mit einem text-Element umschließen

group = data_tree.new_tag('group')
data_tree.teiHeader.insert_after(group)
for text in data_tree.find_all('text'):
    group.append(text)
text = data_tree.new_tag('text')
group.wrap(text)

print('Fertig.')




print('Namensraum wird deklariert und Schemata werden referenziert...')

# Namensraum deklarieren

data_tree.find('TEI')['xmlns'] = "http://www.tei-c.org/ns/1.0"


# Schemata referenzieren

data = str(data_tree)

models = ''

def xmlmod(schema_name):
    if not re.match('.+?\.((xsd)|(rng)|(xml)|(odd)|(sch))',schema_name):
        print('"' + schema_name + '" konnte keinem Schematyp zugeordnet werden. Für das Schema wird keine Referenz im Dokument eingefügt.')
        return ''
    else:
        if re.match('.+?\.xsd',schema_name):
            schematypens = '"http://www.w3.org/2001/XMLSchema"'
            schema_type = '"application/xml"'
        elif re.match('.+?\.rng',schema_name):
            schematypens = '"http://relaxng.org/ns/structure/1.0"'
            schema_type = '"application/xml"'
        elif re.match('.+?\.(xml)|(odd)',schema_name):
            schematypens = '"http://www.tei-c.org/ns/1.0"'
            schema_type = '"application/tei+xml"'
        elif re.match('.+?\.sch',schema_name):
            schematypens = '"http://purl.oclc.org/dsdl/schematron"'
            schema_type = '"application/xml"'
        return '<?xml-model href="' + schema_name + '" type=' + schema_type + ' schematypens=' + schematypens +'?>'
         

for schema in schemas:
    models = models + xmlmod(schema)
    # Schema-Dateien ggf. in output-Ordner kopieren
    if os.path.exists(os.sep.join([file_dir,'input',schema])) == True:
        copyfile(os.sep.join([file_dir,'input',schema]), os.sep.join([file_dir,'output',schema]))


xmldecl = re.compile('<\?xml .+?\?>')
data = xmldecl.sub('\g<0>' + models,data)

print('Fertig.')


# Formatierung des Outputs (newlines neu setzen)

print('Output wird formatiert...')

lines = []

def set_newlines(match):
    if match.group(0) == match.group(1):
        return '\n' + match.group(0)
    elif match.group(0) == match.group(2):
        return match.group(0) + '\n'
    elif match.group(0) == match.group(3):
        return '\n' + match.group(0) + '\n'
        
newlines = re.compile('((?:<lb/>)|(?:</?table.*?>)|(?:</row>)|(?:</figure>)|(?:</figDesc>)|(?:</?p>)|(?:</ab>)|(?:</body>))|((?:<figDesc.*?>)|(?:<\?.*?>)|(?:</?group.*?>))|((?:<figure>)|(?:</?TEI.*?>)|(?:<pb.*?/>)|(?:</?text.*?>)|(?:<row.*?>))')

for line in data.splitlines():
    if line == '':
        continue
    else:
        line_splitted = line.split()
        line_stripped = ' '.join(line_splitted)
        line_new = newlines.sub(set_newlines,line_stripped)
        lines.append(line_new)

data = ' '.join(lines)

# Header einfügen
data = re.sub('<teiHeader>.+</teiHeader>',new_header,data,re.S)

# Entitäten korrekt kodieren
data = re.sub(r'\\U([A-Za-z0-9]+)','&#\g<1>;',data)

print('Fertig.')

print('Daten werden geschrieben.')

tei_out.write(data)

print('Fertig.')

tei_out.close()

print('Die TEI-konformen Daten befinden sich jetzt im output-Ordner.')